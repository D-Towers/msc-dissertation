﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


///<summary> Class <c>preserveMusic </c> keeps the object the script is attached to from being destroyed between scenes. Ensures only one
///GameObject with the music tag is kept. </summary>
///
public class preserveMusic : MonoBehaviour
{
    ///<summary> Called when script instance is being loaded. Sets object to not be destroyed when scenes change. 
    ///if another object with the music tag exits one is destroyed to ensure only one is active. </summary>
    ///
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        GameObject[] musicPlayers = GameObject.FindGameObjectsWithTag("music");
        if (musicPlayers.Length > 1)
        {
            for (int i = 0; i < musicPlayers.Length; i++)
            {
                if (musicPlayers[i] != this.gameObject)
                {
                    if (gameObject.GetComponent<AudioSource>().clip == musicPlayers[i].GetComponent<AudioSource>().clip)
                        Destroy(this.gameObject);
                    else
                        Destroy(musicPlayers[i]);
                }
            }
        }
    }
}
