﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>TapSoundScript </c> plays the sound of the tap being turned on. </summary>
///
public class TapSoundScript : MonoBehaviour
{
    [SerializeField] private AudioClip m_tapOnSound;
    [SerializeField] private AudioClip m_tapOffSound;
    [SerializeField] private WaterEmitter m_waterEmitter;

    private AudioSource audioSource;
    private bool playOn = true;

    ///<summary>Called once when class is initialised. Initialises audioSource for later use.</summary>
    ///
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    /// <summary> Called when the Collider other enters the trigger. When triggered plays sound of turning the tap on or off. </summary>
    /// <param name="other"> The other Collider involved in this collision. </param>
    ///
    private void OnTriggerEnter(Collider other) 
    {
        if (playOn)
            audioSource.clip = m_tapOnSound;
        else
            audioSource.clip = m_tapOffSound;
        audioSource.Play();
        playOn = !playOn;
        m_waterEmitter.swapPosition();
    }

    public AudioClip getM_tapOnSound()
    {
        return this.m_tapOnSound;
    }

    public void setM_tapOnSound(AudioClip m_tapOnSound)
    {
        this.m_tapOnSound = m_tapOnSound;
    }

    public AudioClip getM_tapOffSound()
    {
        return this.m_tapOffSound;
    }

    public void setM_tapOffSound(AudioClip m_tapOffSound)
    {
        this.m_tapOffSound = m_tapOffSound;
    }

    public WaterEmitter getM_waterEmitter()
    {
        return this.m_waterEmitter;
    }

    public void setM_waterEmitter(WaterEmitter m_waterEmitter)
    {
        this.m_waterEmitter = m_waterEmitter;
    }
}
