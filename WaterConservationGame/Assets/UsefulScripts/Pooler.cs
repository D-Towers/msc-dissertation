﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>Pooler </c> creates an amount of a set GameObject at initialisation to be enable rather than initialising live. </summary>
///
public class Pooler : MonoBehaviour
{
    public static Pooler SharedInstance;

    private List<GameObject> pooledObjects;

    [SerializeField] private GameObject m_objectToPool;
    [SerializeField] private int m_amountToPool;

    ///<summary> Called when script instance is being loaded. Sets static variable to be accessed </summary>
    ///
    void Awake() {
        SharedInstance = this;
    }

    ///<summary> Called once when class is initialised. Creates a pool of objects, disables them and adds them to a list. </summary>
    ///
    void Start()
    {
        pooledObjects = new List<GameObject>();
        for(int i = 0; i < m_amountToPool; i++) 
        {
            GameObject obj = (GameObject) Instantiate(m_objectToPool);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
    }

    ///<summary> Retrieves one of the pooled objects out of the list if a disabled one is available </summary>
    ///<returns> A disbled GameObject in the pooledObjects list </returns>
    ///
    public GameObject GetPooledObject() {
        for (int i = 0; i < pooledObjects.Count; i++) 
        {
            if (!pooledObjects[i].activeInHierarchy)
                return pooledObjects[i];
        }
        return null;
    }

    public GameObject getM_objectToPool()
    {
        return this.m_objectToPool;
    }

    public void setM_objectToPool(GameObject m_objectToPool)
    {
        this.m_objectToPool = m_objectToPool;
    }

    public int getM_amountToPool()
    {
        return this.m_amountToPool;
    }

    public void setM_amountToPool(int m_amountToPool)
    {
        this.m_amountToPool = m_amountToPool;
    }

    public List<GameObject> getPooledObjects()
    {
        return this.pooledObjects;
    }
}
