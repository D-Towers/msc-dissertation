﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>DestroyByBoundary </c> disables objects that have collided once they leave. </summary>
///
public class DestroyByBoundary : MonoBehaviour
{
    [SerializeField] private string m_targetTag;

    /// <summary> Called when the Collider other has stopped touching the trigger. Disables targets that exit with matching tag. </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    ///
    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == m_targetTag)
            other.gameObject.SetActive(false);
    }

    public string getM_targetTag()
    {
        return this.m_targetTag;
    }

    public void setM_targetTag(string m_targetTag)
    {
        this.m_targetTag = m_targetTag;
    }
}
