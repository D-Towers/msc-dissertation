﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>musicScript </c> is a script that when attached to an object with an AudioSource, will adjust the volume to be equal
///to the volume stored in the component multiplied by the player chosen volume. </summary>
///
public class musicScript : MonoBehaviour
{
    private AudioSource audioSource;
    private float baseVol;
    private bool wasPlaying = false;

    ///<summary> Called once when class is initialised. Initialises variables for later use. </summary>
    ///
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        baseVol = audioSource.volume;
    }

    ///<summary> Called once per frame. Changes the volume to match the players preference. stops music when timeScale is 0. </summary>
    ///
    void Update()
    {
        float volSettings = PlayerPrefs.GetFloat("Volume", 1f);
        audioSource.volume = baseVol * volSettings;
        if (Time.timeScale == 0 && audioSource.isPlaying)
        {
            wasPlaying = true;
            audioSource.Pause();
        }
        else if (Time.timeScale > 0 && !audioSource.isPlaying && wasPlaying)
        {
            audioSource.Play();
            wasPlaying = false;
        }
    }
}
