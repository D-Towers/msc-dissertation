﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>FadeMove </c> disables objects that have collided once they leave. </summary>
///
public abstract class FadeMove : MonoBehaviour
{
    [SerializeField] float m_timeMultiplier = 1;
    
    private const float TIME_PENALTY = 2.5f;
    protected const float FADE_SPEED_MULT = 2f;
    protected Color col;
    
    enum direction 
    {
        X,
        Y,
        Z
    }

    [SerializeField] private direction m_directionToTravel;
    [SerializeField] private bool m_isPositiveDirection;

    ///<summary> Called once per frame. Moves the object in a direction, and destroys it once completely invisible. </summary>
    ///
    public void Update()
    {
        float change  = (m_isPositiveDirection ? m_timeMultiplier : -m_timeMultiplier) * (Time.deltaTime/TIME_PENALTY);
        gameObject.transform.Translate(new Vector3((m_directionToTravel == direction.X) ? change: 0, (m_directionToTravel == direction.Y) ? change: 0, (m_directionToTravel == direction.Z) ? change: 0));
        Fade();
        if (col.a <= 0)
            Destroy(gameObject);
    }

    ///<summary> Virtual function overwritten by child functions. </summary>
    ///
    public virtual void Fade(){}
}
