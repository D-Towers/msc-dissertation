﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>ObjectChecker </c> sets a bool when the target GameObject has triggered this GameObjects collider. </summary>
///
public class ObjectChecker : MonoBehaviour
{
    [SerializeField] private GameObject m_target;
    private bool triggerOccured = false;

    /// <summary> Called when the Collider other enters the trigger. </summary>
    /// <param name="other"> The other Collider involved in this collision. </param>
    ///
    void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == m_target.transform.name)
            triggerOccured = true;
    }


    /// <summary> Checks whether target has collided with the GameObject. </summary>
    /// <param name="other"> True if targets have collided, false otherwise. </param>
    ///
    public bool ObjectsTriggered()
    {
        return triggerOccured;
    }

    
    public GameObject getM_target()
    {
        return this.m_target;
    }

    public void setM_target(GameObject m_target)
    {
        this.m_target = m_target;
    }
}
