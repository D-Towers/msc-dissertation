﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>preserve </c> keeps the object the script is attached to from being destroyed between scenes. </summary>
///
public class preserve : MonoBehaviour
{
    ///<summary> Called once when class is initialised. Sets object to not be destroyed when scenes change. </summary>
    ///
    void Start()
    {
        DontDestroyOnLoad(this);
    }
}
