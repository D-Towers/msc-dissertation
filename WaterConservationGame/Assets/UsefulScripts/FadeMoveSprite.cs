﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>FadeMoveSprite </c> inherits from <c>FadeMove </c>. This class completes the fade function for sprites. </summary>
///
public class FadeMoveSprite : FadeMove
{
    ///<summary> Changes the color of the Sprite to become more transparent. </summary>
    ///
    public override void Fade()
    {
        col = gameObject.GetComponent<SpriteRenderer>().color;
        gameObject.GetComponent<SpriteRenderer>().color = new Color(col.r, col.g, col.b, col.a - (Time.deltaTime * FADE_SPEED_MULT));
    }
}
