﻿// @Author: David Towers (160243066)
using TMPro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

///<summary> Class <c>ScoreCalc </c> displays and animates the uses score from the game. </summary>
///
public class ScoreCalc : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_scoreNum;
    [SerializeField] private TextMeshProUGUI m_waterNum;
    [SerializeField] private TextMeshProUGUI m_timeNum;
    [SerializeField] private TextMeshProUGUI m_finalNum;
    [SerializeField] private SceneController m_sceneController;
    [SerializeField] private TextMeshProUGUI m_tipBox;
    [SerializeField] private GameObject m_tmpPrefab;
    [SerializeField] private RawImage m_background;
    [SerializeField] private List<TextMeshProUGUI> m_textFadeables;
    [SerializeField] private AudioClip pointIncSound;
    [SerializeField] private AudioClip pointDecSound;
    [SerializeField] private int m_waterPar;
    [SerializeField] private string m_waterTip;
    [SerializeField] private int m_scorePar;
    [SerializeField] private string m_scoreTip = "";
    [SerializeField] private int sceneToLoad;
    [SerializeField] private string scoreKey;

    private TextMeshProUGUI[] TMParr;
    private int[] ScoreLimits;
    private AudioSource audioSource;
    private int counter, finalScore = 0;
    private float timePassed = 0;
    private bool skip = false;
    private target currTar = target.score;

    private enum target {
        score,
        water,
        time,
        final
    }

    ///<summary> Called when script instance is being loaded. Sets variables for later. </summary>
    ///
    void  Awake() 
    {
        TMParr = new TextMeshProUGUI[4]{m_scoreNum, m_waterNum, m_timeNum, m_finalNum};
        ScoreLimits = new int[4]{m_sceneController.getScore(), m_sceneController.getWaterUsedRounded(),
                                 m_sceneController.getTimeRemainingRounded(), m_sceneController.getFinalScore()};
        m_background.color = makeColTransparent(m_background.color);
        foreach (TextMeshProUGUI tm in m_textFadeables)
        {
            tm.color = makeColTransparent(tm.color);
        }
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    ///<summary> Called once per frame. Fades in the screen, then displays score, then loads the next scene. </summary>
    ///
    void Update()
    {
        if(m_background.color.a < 1)
            makeVisible();
        else if ((int) currTar < 3)
            calculateScore();
        else
        {
            setTip();
            waitThenLoad();
        }
    }

    ///<summary> Displays change in score to limit. </summary>
    ///
    void calculateScore()
    {
        int limit = ScoreLimits[(int) currTar];
        if ((counter < limit || limit == 0) && !skip)
        {
            string extra = "";
            changeAmounts(incrementCounter(limit));
            if (currTar == target.water)
                extra = "ml";
            if(currTar == target.time)
                extra = "s x 10";
            TMParr[(int) currTar].text += extra;
            if (limit == 0)
                skip = true;
            playSound();
        }
        else if(notPaused())
            moveOnToNext();
    }

    ///<summary> Makes the screen visible over time. </summary>
    ///
    void makeVisible()
    {
        m_background.color = makeColOpaque(m_background.color);
        foreach (TextMeshProUGUI tm in m_textFadeables)
            tm.color = makeColOpaque(tm.color);
    }

    ///<summary> Changes the alpha of given Color <paramref name="old"/> to 0. </summary>
    ///<param name="old"> Color to make transparent. </param>
    ///<returns> A color equal to the original, but with an alpha of 0 </returns>
    ///
    Color makeColTransparent(Color old)
    {
        old.a = 0;
        return old;
    }

    ///<summary> Changes the alpha of given Color <paramref name="old"/> to 1 over time. </summary>
    ///<param name="old"> Color to make opaque. </param>
    ///<returns> A color equal to the original, but with an increased alpha. </returns>
    ///
    Color makeColOpaque(Color old)
    {
        old.a += Time.deltaTime;
        return old;
    }

    ///<summary> Displays a tip on how to imrpove score based on set pars </summary>
    ///
    void setTip()
    {
        string advice = "";
        if (m_sceneController.getWaterUsedRounded() > m_waterPar)
            advice = m_waterTip;
        else if (m_sceneController.getScore() < m_scorePar && m_scoreTip != "")
            advice = m_scoreTip;
        else
            advice = "You did well, try improving your score as well as your speed!";
        m_tipBox.SetText("Tip - " + advice);
    }

    ///<summary> Switches to the next part of the score to apply. </summary>
    ///
    void moveOnToNext()
    {
        if (currTar == target.time || currTar == target.water)
            displayFloat();
        counter = 0;
        currTar++;
        skip = false;
        timePassed = 0;
    }

    ///<summary> Plays the appropriate sound when the points either go up or down. </summary>
    ///
    void playSound()
    {
        if(currTar == target.water)
            audioSource.clip = pointDecSound;
        else
            audioSource.clip = pointIncSound;
        if (!audioSource.isPlaying)
            audioSource.Play();
    }

    ///<summary> Changes the current field and finalScore by <paramref name="change"/>. </summary>
    ///<param name="change"> The integer value to change the final score and current variable by. </param>
    ///
    void changeAmounts(int change)
    {
            counter += change;
            TMParr[(int) currTar].text = counter.ToString();
            if (currTar == target.water)
                change *= -1;
            else if (currTar == target.time)
                change *= 10;
            finalScore += change;
            TMParr[3].text = finalScore.ToString(); 
    }

    ///<summary> Calculates the value to change the score by. </summary>
    ///<param name="limit"> The final value of this variable. </param>
    ///<returns> The amount to increment by. </returns>
    ///
    int incrementCounter(int limit) 
    {
        int increment = 1;           
        for (int i = 0; i < counter.ToString().Length - 2; i++)
            increment = (increment * 10) + increment;
        if(limit < 0)
            increment *= -1;
        if (counter + increment > limit)
            increment -= (counter + increment - limit);
        return increment;
    }

    ///<summary> Waits 0.4 seconds then returns true. </summary>
    ///<returns> True after 0.4 seconds, false otherwise. </returns>
    ///
    bool notPaused()
    {
        timePassed += Time.deltaTime;
        return (timePassed > 0.4f);
    }


    ///<summary> Displays a more exact amount of water used and time remaining next to the 
    ///rounded values after their score has been applied. </summary>
    ///
    void displayFloat() 
    {
        GameObject orig = TMParr[(int) currTar].gameObject;
        Vector3 origPos = orig.transform.position;
        Vector3 newPos = new Vector3(origPos.x + 350, origPos.y, origPos.z);
        GameObject obj = Instantiate(m_tmpPrefab, newPos, orig.transform.rotation, orig.transform.parent);
        float acc = 0;
        if (currTar == target.water)
            acc = m_sceneController.getWaterUsed();
        else
            acc = m_sceneController.getTimeRemaining();
        obj.GetComponent<TextMeshProUGUI>().text = acc.ToString();
    }

    ///<summary> Once 3 seconds have passed loads the next scene. </summary>
    ///
    void waitThenLoad() 
    {
        timePassed += Time.deltaTime;
        if (timePassed > 3)
        {
            PlayerPrefs.SetInt(scoreKey, finalScore);
            SceneManager.LoadSceneAsync(sceneToLoad);
        }
    }
}
