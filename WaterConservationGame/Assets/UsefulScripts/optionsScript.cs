﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

///<summary> Class <c>optionsScript </c> controls the functions for the pause menu. </summary>
///
public class optionsScript : MonoBehaviour
{
    [SerializeField] private Slider m_slider;
    [SerializeField] private TextMeshProUGUI m_sliderPercent;

    ///<summary> Is called whenever the GameObject is enabled. sets the timescale to 0 and retrieves the stored volume preference. </summary>
    ///
    void OnEnable()
    {
        Time.timeScale = 0;
        m_slider.value = PlayerPrefs.GetFloat("Volume", 1);
    }

    ///<summary> Called once per frame. Updates volume preference according to slider and resumes if "Escape" is pressed. </summary>
    ///
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            resume();
        slider();
    }

    ///<summary> Sets the stored volume to be equal to the sliders position, and displays the percentage. </summary>
    ///
    void slider() 
    {
        PlayerPrefs.SetFloat("Volume", m_slider.value);
        m_sliderPercent.SetText(((int) (m_slider.value * 100)).ToString() + "%");
    }

    ///<summary> Resets the timeScale and disables the GameObject. </summary>
    ///
    public void resume()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }

    public Slider GetSlider()
    {
        return m_slider;
    }

    public void SetSlider(Slider sl)
    {
        m_slider = sl;
    }

    public TextMeshProUGUI GetSliderPercent()
    {
        return m_sliderPercent;
    }

    public void SetSliderPerecent(TextMeshProUGUI sl)
    {
        m_sliderPercent = sl;
    }
}
