﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

///<summary> Class <c>FadeMoveTMP </c> inherits from <c>FadeMove </c>. This class completes the fade function for TextMeshProUGUI. </summary>
///
public class FadeMoveTMP : FadeMove
{
    ///<summary> Changes the color of the TextMeshProUGUI to become more transparent. </summary>
    ///
    public override void Fade()
    {
        col = gameObject.GetComponent<TextMeshProUGUI>().color;
        gameObject.GetComponent<TextMeshProUGUI>().color = new Color(col.r, col.g, col.b, col.a - (Time.deltaTime * FADE_SPEED_MULT));
    }
}
