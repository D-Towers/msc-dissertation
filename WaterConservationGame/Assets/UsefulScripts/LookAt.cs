﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>LookAt </c> points the attached gameObject to look at the given taregt. If there is no target, by default, 
///the Main Camera is used instead </summary>
public class LookAt : MonoBehaviour
{
    [SerializeField] private GameObject m_target;

    ///<summary> Called once per frame. makes GameObject look at target. </summary>
    ///
    void Update()
    {
        if (m_target == null)
            m_target = Camera.main.gameObject;
        gameObject.transform.LookAt(m_target.transform);
    }

    public void setM_target(GameObject m_target)
    {
        this.m_target = m_target;
    }
}
