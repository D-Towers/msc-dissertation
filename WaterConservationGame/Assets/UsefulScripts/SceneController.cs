﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

///<summary> Class <c>SceneController </c> is an abstract class that contains methods useful for sceneControllers. </summary>
///
public abstract class SceneController : MonoBehaviour
{
    protected int gameScore = 0;
    protected float waterUsed, waterTime = 0;
    protected bool canEnd = false;
    private bool running = true;
    
    [SerializeField] protected float timeRemaining = 0;
    [SerializeField] protected Animator animator;
    [SerializeField] protected GameObject m_endScreen;
    [SerializeField] protected float m_litresPerMin;
    [SerializeField] protected TextMeshProUGUI m_cleanliness;
    [SerializeField] protected TextMeshProUGUI m_timer;
    [SerializeField] protected TextMeshProUGUI m_score;
    [SerializeField] protected WaterEmitter m_waterEmitter;
    [SerializeField] private GameObject m_scoreChange;
    [SerializeField] private GameObject m_pauseMenu;

    protected const int SCORE_HEIGHT_CHANGE = 50;

    ///<summary> Called once per frame. If the game is running will decrement the timer and check for pausing. </summary>
    ///
    protected virtual void Update()
    {
        if (ContinueScript.canContinue() && running)
        {
            UpdateTimer();
            if (Input.GetKeyDown(KeyCode.Space))
                m_pauseMenu.SetActive(true);
        }
    }

    ///<summary> Checks whether a flag allowing the scene to end has been set </summary>
    ///<returns> True if the scene can be ended, false otherwise. </returns>
    ///
    public bool canBeEnded()
    {
        return canEnd;
    }

    ///<summary> Sets timeScale to 1, and loads scene. </summary>
    ///<param name="sceneID"> Integer ID of scene to load. </param>
    ///
    public void load(int sceneID)
    {
        if (Time.timeScale == 0)
            Time.timeScale = 1;
        SceneManager.LoadSceneAsync(sceneID);
    }

    ///<summary> Sets the text of the cleanliness field to <paramref name="percent"/>. </summary>
    ///<param name="percent"> Integer value of cleanliness percent. </param>
    ///
    protected void setCleanliness(int percent)
    {
        m_cleanliness.SetText("Cleanliness: " + percent + "%");
    }

    ///<summary> Changes the score by <paramref name="change"/>. </summary>
    ///<param name="change"> Integer value of change to score. </param>
    ///
    public void updateScore(int change) {
        gameScore += change;
        GameObject obj = (GameObject) Instantiate(m_scoreChange, m_score.transform);
        obj.GetComponent<TextMeshProUGUI>().text = ((change > 0) ? "+" : "") + change;
        Vector3 pos = m_score.transform.position;
        obj.transform.SetPositionAndRotation(new Vector3(pos.x, pos.y - SCORE_HEIGHT_CHANGE, pos.z), obj.transform.rotation);
        m_score.text = "Score: " + getScore();
    }

    ///<summary> Decrements timer and converts it to a displayable string. </summary>
    ///
    protected void UpdateTimer()
    {
        timeRemaining -= Time.deltaTime;
        int roundTime = Mathf.RoundToInt(timeRemaining);
        int mins = roundTime / 60;
        int secs = roundTime % 60;
        m_timer.text = "Time Remaining - " + mins + ":" + (secs < 10 ? "0" + secs : secs.ToString());
        if(timeRemaining < 0)
        {
            timeRemaining = 0;
            endScene();
        }
    }
   
    ///<summary> Retrieves the current score. </summary>
    ///<returns> Integer equal to the score. </returns>
    ///
    public int getScore() 
    {
        return gameScore;
    }

    ///<summary> Retrieves the amount of water used in ml. </summary>
    ///<returns> Float equal to the amount of water used. </returns>
    ///
    public float getWaterUsed() 
    {
        return Mathf.Round(waterUsed * 100)/100;
    }

    ///<summary> Retrieves the amount of water used in ml rounded to an integer. </summary>
    ///<returns> Integer equal to the amount of water used after being rounded. </returns>
    ///
    public int getWaterUsedRounded() 
    {
        return Mathf.RoundToInt(getWaterUsed());
    }

    ///<summary> Calculates the amount of water in ml. </summary>
    ///
    protected void calculateWaterUsed() 
    {
        waterUsed =  (m_waterEmitter.getTimeRunning() * (m_litresPerMin/60)) * 1000;
    }

    ///<summary> Retrieves the time remaining in seconds. </summary>
    ///<returns> Float equal to the time remaining. </returns>
    ///
    public float getTimeRemaining() 
    {
        return Mathf.Round(timeRemaining * 100)/100;
    }

    ///<summary> Retrieves the time remaing in seconds rounded to an integer. </summary>
    ///<returns> Integer equal to the time remaing in seconds after being rounded. </returns>
    ///
    public int getTimeRemainingRounded() 
    {
        return Mathf.RoundToInt(getTimeRemaining());
    }

    ///<summary> Retrieves the time remaing in seconds rounded to an integer multiplied by ten to be applied to the final score. </summary>
    ///<returns> Integer equal to the time remaing in seconds after being rounded times by 10. </returns>
    ///
    public int getTimeRemainingRoundedScore() 
    {
        return getTimeRemainingRounded() * 10;
    }

    ///<summary> Calculates the final score using the score, waterUsed and the remaining time. </summary>
    ///<returns> Integer equal to the final score after calulating it. </returns>
    ///
    public int getFinalScore() 
    {
        return getScore() - getWaterUsedRounded() + getTimeRemainingRoundedScore();
    }

    ///<summary> Checks whether the game is running. </summary>
    ///<returns> True if the game is running, else otherwise. </returns>
    ///
    public bool isRunning()
    {
        return running;
    }

    ///<summary> Stops timer from ticking, turns off water and then enables the score screen. </summary>
    ///
    public void endScene() 
    {
        running = false;
        calculateWaterUsed();
        m_endScreen.SetActive(true);
        if (m_waterEmitter.isTapOn())
            m_waterEmitter.swapPosition();
    }
}
