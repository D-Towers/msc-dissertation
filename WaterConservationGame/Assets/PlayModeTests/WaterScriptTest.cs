﻿//@Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>WaterScriptTest </c> runs test associated with WaterScript. </summary>
    ///
    public class WaterScriptTest
    {
        ///<summary> Tests waterScript causes object to move. </summary>
        ///
        [UnityTest]
        public IEnumerator WaterChangesPositionTest()
        {
            GameObject testObject = new GameObject();
            testObject.AddComponent<WaterScript>();
            Vector3 startPos = new Vector3(0, 0, 0);
            testObject.transform.position = startPos;
            yield return null;
            Assert.AreNotEqual(startPos, testObject.transform.position);
        }
    }
}
