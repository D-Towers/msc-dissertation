﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>PoolerTests </c> runs tests associated with the Pooler class. </summary>
    ///
    public class PoolerTests
    {
        GameObject testObject;
        Pooler testPool;

        ///<summary> Creates a gameObject before each test that includes the necessary components to test the Pooler script. </summary>
        ///
        [SetUp]
        public void Setup()
        {
            testObject = new GameObject();
            testObject.SetActive(false);
            testPool = testObject.AddComponent<Pooler>();
            testPool.setM_objectToPool(new GameObject());
            testPool.setM_amountToPool(10);
            testObject.SetActive(true);
        }

        ///<summary> Destroys objects after use </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testPool);
        }

        ///<summary> Tests the pooler generates the correct number of GameObjects. </summary>
        ///
        [UnityTest]
        public IEnumerator PoolsCorrectAmountTest()
        {
            testObject = new GameObject();
            testObject.SetActive(false);
            testPool = testObject.AddComponent<Pooler>();
            testPool.setM_objectToPool(new GameObject());
            int rand = Random.Range(1, 21);
            testPool.setM_amountToPool(rand);
            testObject.SetActive(true);
            yield return null;
            Assert.AreEqual(rand, testPool.getPooledObjects().Count);
        }

        ///<summary> Tests a GameObject is retrievable and is disabled. </summary>
        ///
        [UnityTest]
        public IEnumerator RetrievesDisabledGameobjectTest()
        {
            yield return null;
            GameObject go = testPool.GetPooledObject();
            Assert.IsNotNull(go);
            Assert.False(go.activeSelf);
        }

        ///<summary> Tests if no disabled GameObjects are left, null is returned. </summary>
        ///
        [UnityTest]
        public IEnumerator NullWhenNoMoreObjectsTest()
        {
            yield return null;
            foreach (GameObject itr in testPool.getPooledObjects())
                itr.SetActive(true);
            GameObject go = testPool.GetPooledObject();
            Assert.IsNull(go);
        }
    }
}
