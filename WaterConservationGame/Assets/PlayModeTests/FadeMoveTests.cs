﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using TMPro;

namespace Tests
{
    ///<summary> Test class <c>FadeMoveTests </c> runs tests associated with the FadeMove, FadeMoveSprite and, FadeMoveTM classes. </summary>
    ///
    public class FadeMoveTests
    {
        GameObject testObject;

        ///<summary> Creates a gameObject before each test that includes the necessary components to test the scripts. </summary>
        ///
        [SetUp]
        public void Setup()
        {
            testObject = new GameObject();
        }

        ///<summary> Destroys gameObject after each test to reset it. </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
        }

        ///<summary> Tests <c>FadeMoveSprite <c> moves the object. </summary>
        ///
        [UnityTest]
        public IEnumerator SpriteMakesObjectMove()
        {;
            testObject.AddComponent<SpriteRenderer>();
            testObject.AddComponent<FadeMoveSprite>();
            Vector3 position = testObject.transform.position;
            yield return null;
            Assert.AreNotEqual(position, testObject.transform.position);
        }

        ///<summary> Tests <c>FadeMoveTM <c> moves the object. </summary>
        ///
        [UnityTest]
        public IEnumerator TextMeshMakesObjectMove()
        {;
            testObject.AddComponent<TextMeshProUGUI>();
            testObject.AddComponent<FadeMoveTMP>();
            Vector3 position = testObject.transform.position;
            yield return null;
            Assert.AreNotEqual(position, testObject.transform.position);
        }

        ///<summary> Tests <c>FadeMoveSprite <c> reduces alpha of sprite color. </summary>
        ///
        [UnityTest]
        public IEnumerator SpriteGoesTransparentTest()
        {
            testObject.AddComponent<SpriteRenderer>();
            testObject.AddComponent<FadeMoveSprite>();
            Assert.AreEqual(testObject.GetComponent<SpriteRenderer>().color.a, 1);
            yield return null;
            Assert.AreNotEqual(testObject.GetComponent<SpriteRenderer>().color.a, 1);
        }

        ///<summary> Tests <c>FadeMoveTM <c> reduces alpha of TextMeshProUGUI color. </summary>
        ///
        [UnityTest]
        public IEnumerator TextMeshGoesTransparentTest()
        {
            testObject.AddComponent<TextMeshProUGUI>();
            testObject.AddComponent<FadeMoveTMP>();
            Assert.AreEqual(testObject.GetComponent<TextMeshProUGUI>().color.a, 1);
            yield return null;
            Assert.AreNotEqual(testObject.GetComponent<TextMeshProUGUI>().color.a, 1);
        }
    }
}
