﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using TMPro;

namespace Tests
{
    ///<summary> Test class <c>FinalScriptTest </c> runs test associated with FinalScript. </summary>
    ///
    public class FinalScriptTest
    {
        GameObject testObject;
        finalScript testFinal;

        ///<summary> Sets up gameObjects for test. </summary>
        ///
        [SetUp]
        public void SetUp()
        {
            testObject = new GameObject();
            AudioSource audioSource = testObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            testObject.SetActive(false);

            testFinal = testObject.AddComponent<finalScript>();
            testFinal.setAudioSource(audioSource);
            testFinal.setM_sceneToLoad(-1);
            testFinal.setScoreField(new TextMeshProUGUI());
        }

        ///<summary> Destroys gameObjects after use. </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testFinal);
        }

        ///<summary> tests that the score is properly calculated from PlayerPrefs. </summary>
        ///
        [UnityTest]
        public IEnumerator ScoreIsProperlyCalculatedTest()
        {
            int randBScore = Random.Range(0, 1000);
            PlayerPrefs.SetInt("brushScore", randBScore);
            int randDScore = Random.Range(0, 1000);
            PlayerPrefs.SetInt("dishScore", randDScore);
            testObject.SetActive(true);
            yield return null;
            Assert.AreEqual(randBScore + randDScore, testFinal.getFinalScore());
        }
    }
}
