﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>ObjectCheckerTest </c> runs tests associated with the ObjectChecker class. </summary>
    ///
    public class ObjectCheckerTest
    {
        GameObject testObject;
        GameObject testTrigger;
        GameObject falseTrigger;
        ObjectChecker testChecker;

        Vector3 objPos = new Vector3(0, 0, 0);
        Vector3 trigPos = new Vector3(10, 10, 10);

        ///<summary> Creates a gameObject before each test that includes the necessary components to test the ObjectChecker script. </summary>
        ///
        [SetUp]
        public void SetUp()
        {
            testObject = new GameObject();
            testObject.name = "testObj";
            testObject.transform.position = objPos;
            SphereCollider sc = testObject.AddComponent<SphereCollider>();
            sc.isTrigger = true;
            testChecker = testObject.AddComponent<ObjectChecker>();

            testTrigger = new GameObject();
            testTrigger.name = "testTrig";
            testTrigger.transform.position = trigPos;
            testTrigger.AddComponent<Rigidbody>();
            SphereCollider tsc = testTrigger.AddComponent<SphereCollider>();
            tsc.isTrigger = true;

            falseTrigger = new GameObject();
            falseTrigger.name = "falsTrig";
            falseTrigger.transform.position = trigPos;
            falseTrigger.AddComponent<Rigidbody>();
            SphereCollider fsc = falseTrigger.AddComponent<SphereCollider>();
            fsc.isTrigger = true;

            testChecker.setM_target(testTrigger);
        }

        ///<summary> Destroys objects after use. </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testTrigger);
            GameObject.Destroy(falseTrigger);
            GameObject.Destroy(testChecker);
        }

        ///<summary> Tests the bool changes when the object is triggered. </summary>
        ///
        [UnityTest]
        public IEnumerator TriggerChangesBoolTest()
        {
            Assert.False(testChecker.ObjectsTriggered());
            testTrigger.transform.position = objPos;
            yield return null;
            Assert.True(testChecker.ObjectsTriggered());
            testTrigger.transform.position = trigPos;
            yield return null;
            Assert.True(testChecker.ObjectsTriggered());
        }

        ///<summary> Tests that only the target an trigger. </summary>
        ///
        [UnityTest]
        public IEnumerator NonTargetWontTriggerTest()
        {
            Assert.False(testChecker.ObjectsTriggered());
            falseTrigger.transform.position = objPos;
            yield return null;
            Assert.False(testChecker.ObjectsTriggered());
        }
    }
}
