﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>LookAtTest </c> runs the test associated with the LookAt class. </summary>
    ///
    public class LookAtTest
    {

        GameObject MainCamera;
        GameObject testObject;
        LookAt testLook;

        ///<summary> Creates a gameObject before each test that includes the necessary components to test the LookAt script.</summary>
        ///
        [SetUp]
        public void Setup()
        {
            MainCamera = new GameObject();
            MainCamera.tag = "MainCamera";
            MainCamera.AddComponent<Camera>();
            MainCamera.AddComponent<AudioListener>();
            MainCamera.AddComponent<SphereCollider>();
            MainCamera.transform.position = new Vector3(20, 20, 20);

            testObject = new GameObject();
            testObject.SetActive(false);
            testLook = testObject.AddComponent<LookAt>();
            testObject.transform.position = new Vector3(0, 0, 0);
        }

        ///<summary> Destroys objects after use. </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(MainCamera);
            GameObject.Destroy(testObject);
            GameObject.Destroy(testLook);
        }

        ///<summary> Tests a GameObject will look at target once script is applied. </summary>
        ///
        [UnityTest]
        public IEnumerator PointsToTargetTest()
        {
            GameObject testTarget = new GameObject();
            string tarName = "Target";
            testTarget.name = tarName;
            testTarget.AddComponent<SphereCollider>();
            testTarget.transform.position = new Vector3(10, 10, 10);
            
            testLook.setM_target(testTarget);
            
            RaycastHit hit;
            if (Physics.Raycast(testObject.transform.position, testObject.transform.forward, out hit, Mathf.Infinity))
                Assert.Fail();
            
            testObject.SetActive(true);
            yield return null;
            if (Physics.Raycast(testObject.transform.position, testObject.transform.forward, out hit, Mathf.Infinity))
                Assert.AreEqual(tarName, hit.transform.gameObject.name);
            else
                Assert.Fail();
        }

        ///<summary> Tests a GameObject will look at the camera if no target is specified. </summary>
        ///
        [UnityTest]
        public IEnumerator DefaultsToCameraTest()
        {            
            RaycastHit hit;
            if (Physics.Raycast(testObject.transform.position, testObject.transform.forward, out hit, Mathf.Infinity))
                Assert.Fail();
            
            testObject.SetActive(true);
            yield return null;
            if (Physics.Raycast(testObject.transform.position, testObject.transform.forward, out hit, Mathf.Infinity))
                Assert.AreEqual(MainCamera, hit.transform.gameObject);
            else
                Assert.Fail();
        }
    }
}
