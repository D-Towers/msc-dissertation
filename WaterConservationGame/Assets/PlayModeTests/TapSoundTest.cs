﻿//@Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>TapSoundTest </c> runs tests associated with TapSoundScript. </summary>
    ///
    public class TapSoundTest
    {
        AudioClip clip1 = AssetDatabase.LoadAssetAtPath<AudioClip>("Assets/Audio/SFX/Edited Tap On.wav");
        AudioClip clip2 = AssetDatabase.LoadAssetAtPath<AudioClip>("Assets/Audio/SFX/Edited Tap Off.wav");

        //This clip is used to make sure that when testing if the clip plays, it lasts long enough that it doesn't finish before the next frame.
        AudioClip clip3 = AssetDatabase.LoadAssetAtPath<AudioClip>("Assets/Audio/Music Tracks/InfiniteDoors.wav");

        GameObject testObject;
        AudioSource testAudio;
        TapSoundScript testTST;
        GameObject triggerObject;
        Pooler testPool;

        Vector3 objPos = new Vector3(0, 0, 0);
        Vector3 trigPos = new Vector3(10, 10, 10);
        
        
        ///<summary> Creates a gameObject before each test that includes the necessary components to test TapSoundScript. </summary>
        ///
        [SetUp]
        public void Setup()
        {
            testObject = new GameObject();
            testObject.SetActive(false);
            testObject.transform.position = objPos;
            testAudio = new AudioSource();
            testAudio = testObject.AddComponent<AudioSource>();
            testAudio.clip = clip1;
            testAudio.volume = 0;
            testAudio.playOnAwake = false;
            testObject.SetActive(true);

            //Required by WaterEmitter which is required by TapSoundScript
            testPool = testObject.AddComponent<Pooler>();
            testPool.setM_objectToPool(new GameObject());
            testPool.setM_amountToPool(10);

            //Rewuired by TapSoundScript
            WaterEmitter we = testObject.AddComponent<WaterEmitter>();
            we.setM_timeBetweenEmits(1);

            testTST = testObject.AddComponent<TapSoundScript>();
            testTST.setM_tapOnSound(clip1);
            testTST.setM_tapOffSound(clip2);
            testTST.setM_waterEmitter(we);

            SphereCollider sc = testObject.AddComponent<SphereCollider>();
            sc.isTrigger = true;

            triggerObject = new GameObject();
            triggerObject.transform.position = trigPos;
            triggerObject.AddComponent<Rigidbody>();
            SphereCollider tsc = triggerObject.AddComponent<SphereCollider>();
            tsc.isTrigger = true;
        }

        ///<summary> Destroys objects after use </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testAudio);
            GameObject.Destroy(testTST);
            GameObject.Destroy(triggerObject);
            GameObject.Destroy(testPool);
        }

        ///<summary> Tests sound plays when triggered. </summary>
        ///Test sometimes appears to fail, depending on how long yeild return null skips
        ///
        [UnityTest]
        public IEnumerator PlayOnTriggerTest()
        {
            testAudio.clip = clip3;
            Assert.False(testAudio.isPlaying);
            triggerObject.transform.position = objPos;
            yield return null;
            Assert.True(testAudio.isPlaying);
        }

        ///<summary> Tests the sound switches after playing. </summary>
        ///
        [UnityTest]
        public IEnumerator PlayDifferentSoundTest()
        {
            triggerObject.transform.position = objPos;
            yield return new  WaitForSeconds(1);
            Assert.AreEqual(clip1.name, testAudio.clip.name);
            testAudio.Stop();
            triggerObject.transform.position = trigPos;
            yield return new WaitForSeconds(1);

            triggerObject.transform.position = objPos;
            yield return new  WaitForSeconds(1);;
            Assert.AreEqual(clip2.name, testAudio.clip.name);
            testAudio.Stop();
            triggerObject.transform.position = trigPos;
            yield return new  WaitForSeconds(1);

            triggerObject.transform.position = objPos;
            yield return new  WaitForSeconds(1);
            Assert.AreEqual(clip1.name, testAudio.clip.name);
            testAudio.Stop();
        }
    }
}
