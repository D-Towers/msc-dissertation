﻿//@Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>StainScriptTests </c> runs tests associated with StainScript. </summary>
    ///
    public class StainScriptTests
    {
        GameObject testObject;
        stainScript testStain;

        ///<summary> Creates gameObjects before each test that includes the necessary components to test StainScript. </summary>
        ///
        [SetUp]
        public void SetUp()
        {
            testObject = new GameObject();
            testObject.AddComponent<SpriteRenderer>();
            testObject.AddComponent<BoxCollider>();
            testStain = testObject.AddComponent<stainScript>();
        }

        ///<summary> Destroys objects after use </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testStain);
        }

        ///<summary> Tests that the alpha of the SpriteRenderer is reduced by the amount passed through. </summary>
        ///
        [UnityTest]
        public IEnumerator CleanTest()
        {
            float randClean = Random.Range(0, 1);
            testStain.clean(randClean);
            yield return null;
            Assert.AreEqual(1 - randClean, testStain.getCurrColor().a);
        }

        
        ///<summary> Tests that the stains have their color set back to the original color. </summary>
        ///
        [UnityTest]
        public IEnumerator ResetTest()
        {
            testStain.clean(0.5f);
            yield return null;
            Assert.AreNotEqual(testStain.getStartColor(), testStain.getCurrColor());
            testStain.reset();
            Assert.AreEqual(testStain.getStartColor(), testStain.getCurrColor());
        }

        ///<summary> Tests that the BoxCollider is disabled when the alpha is 0. </summary>
        ///
        [UnityTest]
        public IEnumerator BoxColliderDisablesWhenInvisibleTest()
        {
            Assert.True(testObject.GetComponent<BoxCollider>().enabled);
            testStain.clean(1);
            yield return null;
            Assert.AreEqual(0,  testStain.getCurrColor().a);
            Assert.False(testObject.GetComponent<BoxCollider>().enabled);
        }
    }
}
