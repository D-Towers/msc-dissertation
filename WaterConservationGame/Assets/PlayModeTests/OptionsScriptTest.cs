﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.TestTools;
using TMPro;

namespace Tests
{
    ///<summary> Test class <c>optionsScriptTest </c> runs tests associated with the options Script. </summary>
    ///
    public class OptionsScriptTest
    {
        GameObject testObject;
        optionsScript os;

        ///<summary> Creates a gameObject before each test that includes the necessary components to test the options script. </summary>
        ///
        [SetUp]
        public void Setup()
        {
            testObject = new GameObject();
            testObject.SetActive(false);
            os = testObject.AddComponent<optionsScript>();
            os.SetSlider(testObject.AddComponent<Slider>());
            os.SetSliderPerecent(testObject.AddComponent<TextMeshProUGUI>());
            PlayerPrefs.SetFloat("Volume", 1);
            testObject.SetActive(true);
        }

        ///<summary> Destroys GameObjects after use. And resets timeScale. </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(os);
            Time.timeScale = 1;
        }

        ///<summary> Destroys gameObject after each test to reset it. </summary>
        ///
        [Test]
        public void StartTimeScaleTest()
        {
            Assert.AreEqual(0, Time.timeScale);
        }

        ///<summary> Tests the resume function resetting timeScale. </summary>
        ///
        [Test]
        public void ResumeTimeScaleTest()
        {
            Time.timeScale = 0;
            Assert.AreEqual(0, Time.timeScale);
            os.resume();
            Assert.AreEqual(1, Time.timeScale);
        }

        ///<summary> Tests the resume function deactivating the GameObject. </summary>
        ///
        [Test]
        public void ResumeTestActive()
        {
            testObject.SetActive(true);
            Assert.AreEqual(true, testObject.activeSelf);
            os.resume();
            Assert.AreNotEqual(true,  testObject.activeSelf);
        }

        ///<summary> Tests the slider is set to the value of the player preference when enabled. </summary>
        ///
        [Test]
        public void StartChangePreferenceTest()
        {
            testObject.SetActive(false);
            PlayerPrefs.SetFloat("Volume", 0.5f);
            testObject.SetActive(true);
            Assert.AreEqual(0.5f, os.GetSlider().value);
        }

        ///<summary> Tests the player preference has changed when the slider value has changed. </summary>
        ///
        [UnityTest]
        public IEnumerator VolumeChangePreferenceTest()
        {
            Assert.AreEqual(1, os.GetSlider().value);
            os.GetSlider().value = 0.75f;
            yield return null;
            Assert.AreEqual(0.75f, os.GetSlider().value);
        }
    }
}
