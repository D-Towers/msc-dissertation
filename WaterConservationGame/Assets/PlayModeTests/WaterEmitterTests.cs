﻿//@Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>WaterEmitterTests </c> runs tests associated with WaterEmitter class. </summary>
    ///
    public class WaterEmitterTests
    {
        GameObject testObject;
        WaterEmitter testEmitter;

        ///<summary> Takes in a gameObject and adds objects to ensure the correct components are there to add a WaterEmitter. </summary>
        ///
        public static GameObject addWaterEmitterParameters(GameObject testObject)
        {
            
            return testObject;
        }

        ///<summary> Creates a gameObjects before each test that includes the necessary components to test WaterEmitter. </summary>
        ///
        [SetUp]
        public void SetUp()
        {
            testObject = new GameObject();
            testObject.SetActive(false);
            AudioSource audioSource = testObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            audioSource.clip = AssetDatabase.LoadAssetAtPath<AudioClip>("Assets/Audio/SFX/Edited Tap On.wav");
            Pooler testPooler = testObject.AddComponent<Pooler>();
            testPooler.setM_objectToPool(new GameObject());
            testPooler.setM_amountToPool(5);
            testEmitter = testObject.AddComponent<WaterEmitter>();
            testEmitter.setM_timeBetweenEmits(0.1f);
            testObject.SetActive(true);
        }

        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testEmitter);
        }

        ///<summary> Tests the emitter swaps between the two positions. </summary>
        ///
        [UnityTest]
        public IEnumerator PositionWillSwapTest()
        {
            Assert.False(testEmitter.isTapOn());
            testEmitter.swapPosition();
            Assert.True(testEmitter.isTapOn());
            yield return null;
        }

        ///<summary> Tests the emitter emits the gameObjects from the pooler at the interval. </summary>
        ///
        [UnityTest]
        public IEnumerator WaterIsEmittedTest()
        {
            testEmitter.swapPosition();
            List<GameObject> list = Pooler.SharedInstance.getPooledObjects();
            for (int i = 1; i <+ list.Count; i++)
            {
                yield return new WaitForSeconds(0.1f);
                int numActive = 0;
                foreach (GameObject go in list)
                {
                    if (go.activeSelf)
                        numActive++;
                }
                Assert.AreEqual(i, numActive);
            }
        }

        ///<summary> Tests the emitter does not emit when off. </summary>
        ///
        [UnityTest]
        public IEnumerator WaterIsNotEmittedWhenOffTest()
        {
            Assert.False(testEmitter.isTapOn());
            List<GameObject> list = Pooler.SharedInstance.getPooledObjects();
            for (int i = 1; i <+ list.Count; i++)
            {
                yield return new WaitForSeconds(0.1f);
                int numActive = 0;
                foreach (GameObject go in list)
                {
                    if (go.activeSelf)
                        numActive++;
                }
                Assert.AreEqual(0, numActive);
            }
        }
    }
}
