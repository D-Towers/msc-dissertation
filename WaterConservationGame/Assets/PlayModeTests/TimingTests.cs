﻿//@Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>TimingTests </c> runs tests associated with Timing. </summary>
    ///
    public class TimingTests
    {
        GameObject testObject;
        Timing testTiming;


        ///<summary> Creates gameObjects before each test that includes the necessary components to test Timing. </summary>
        ///
        [SetUp]
        public void SetUp()
        {
            testObject = new GameObject();
            testObject.SetActive(false);
            testTiming = testObject.AddComponent<Timing>();
            testObject.AddComponent<SwapVisibility>();
            testTiming.setM_badSprite(AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Custom Assets/ScoreTitles/Prefabs/badPre.prefab"));
            testTiming.setM_excellentSprite(AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Custom Assets/ScoreTitles/Prefabs/excellentPre.prefab"));
            testTiming.setM_goodSprite(AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Custom Assets/ScoreTitles/Prefabs/goodPre.prefab"));
            testTiming.setM_missedSprite(AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Custom Assets/ScoreTitles/Prefabs/missedPre.prefab"));
            testTiming.setM_okaySprite(AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Custom Assets/ScoreTitles/Prefabs/okayPre.prefab"));
            testTiming.setM_perfectSprite(AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Custom Assets/ScoreTitles/Prefabs/perfectPre.prefab"));

            //Because the script deactivates itself in Start() two set actives are needed, as it will deactivate after the first activation
            testObject.SetActive(true);
        }

        ///<summary> Destroys objects after use </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testTiming);
        }

        ///<summary> Tests that the localScale is reset to be equal to the baseScale. </summary>
        ///
        /*When Asserting I am using ToString because, using the base Vector3s the test will fail stating 
        ("Expected: (2.5, 2.5, 1.0)\n But was:  (2.5, 2.5, 1.0)") which should pass the test, as they are equal*/
        [UnityTest]
        public IEnumerator ResetTest()
        {
            testObject.SetActive(true);
            testObject.transform.localScale = new Vector3(1, 1, 1);
            Assert.AreNotEqual(testObject.transform.localScale.ToString(), testTiming.getResetScale().ToString());
            testTiming.reset();
            yield return null;
            Assert.AreEqual(testObject.transform.localScale.ToString(), testTiming.getResetScale().ToString());
        }

        ///<summary> Tests that the localScale is reduced every frame. </summary>
        ///
        [UnityTest]
        public IEnumerator ShrinkTest()
        {
            testObject.SetActive(true);
            testObject.transform.localScale = new Vector3(2,2,2);
            float startX = testObject.transform.localScale.x;
            yield return null;
            Assert.Less(testObject.transform.localScale.x, startX);
        }

        ///<summary> Tests that the turnOff function disables the GameObject. </summary>
        ///
        [UnityTest]
        public IEnumerator turnOffTest()
        {
            testObject.SetActive(true);
            Assert.True(testObject.activeSelf);
            yield return null;
            Assert.True(testObject.activeSelf);
            testTiming.turnOff();
            Assert.False(testObject.activeSelf);
        }
    }
}
