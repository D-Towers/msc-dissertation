﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>MusicScriptTest </c> runs tests associated with MusicScript. </summary>
    ///
    public class MusicScriptTest
    {
        AudioClip clip = AssetDatabase.LoadAssetAtPath<AudioClip>("Assets/Audio/SFX/Edited Tap On.wav");


        ///<summary> Tests that the script changes the volume of the AudioSource with respect to the Player Preference. </summary>
        ///
        [UnityTest]
        public IEnumerator VolumeChangesToMatchPrefTest()
        {
            PlayerPrefs.SetFloat("Volume", 1);
            GameObject testObject = new GameObject();
            testObject.SetActive(false);

            AudioSource testAudio = testObject.AddComponent<AudioSource>();
            testAudio.playOnAwake = false;
            testAudio.clip = clip;
            float baseVol = Random.Range(0, 1);
            testAudio.volume = baseVol;

            testObject.AddComponent<musicScript>();
            testObject.SetActive(true);

            Assert.AreEqual(baseVol, testAudio.volume);
            float mult = Random.Range(0, 1);
            PlayerPrefs.SetFloat("Volume", mult);
            yield return null;

            Assert.AreEqual(baseVol * mult, testAudio.volume);
        }
    }
}
