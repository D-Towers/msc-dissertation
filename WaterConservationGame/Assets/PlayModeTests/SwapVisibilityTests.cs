﻿//@Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{

    ///<summary> Test class <c>SwapVisibilityTests </c> runs tests associated with Swap Visibility. </summary>
    ///
    public class SwapVisibilityTests
    {
        GameObject testObject;
        SpriteRenderer testRender;
        SwapVisibility testVis;

        Color trans = new Color(1, 1, 1, 0);
        Color opaque = new Color(1, 1, 1, 1);

        public static GameObject addSwapVisibilityParameters(GameObject testObject)
        {
            testObject.AddComponent<SpriteRenderer>();
            testObject.AddComponent<SwapVisibility>();
            return testObject;
        }

        ///<summary> Creates gameObjects before each test that includes the necessary components to test SwapVisibility. </summary>
        ///
        [SetUp]
        public void SetUp()
        {
            testObject = addSwapVisibilityParameters(new GameObject());
            testRender = testObject.GetComponent<SpriteRenderer>();
            testVis = testObject.GetComponent<SwapVisibility>();
        }

        ///<summary> Destroys objects after use </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testRender);
            GameObject.Destroy(testVis);
        }

        ///<summary> Tests that the swap function will make opaque objects start to fade out. </summary>
        ///
        [UnityTest]
        public IEnumerator SwapMakesOpaqueGoTransparentTest()
        {
            testRender.color = opaque;
            testVis.swap();
            yield return null;
            Assert.Less(testRender.color.a, opaque.a);
        }

        ///<summary> Tests that the swap function will make opaque objects start to fade in. </summary>
        ///
        [UnityTest]
        public IEnumerator SwapMakesTransparentGoOpaqueTest()
        {
            testRender.color = trans;
            testVis.swap();
            yield return null;
            Assert.Greater(testRender.color.a, trans.a);
        }

        ///<summary> Tests that setVisible will start an object to fade in or out. </summary>
        ///
        [UnityTest]
        public IEnumerator SetVisibleStartsSwappingTest()
        {
            testRender.color = trans;
            testVis.setVisibilty(true);
            yield return null;
            Assert.Greater(testRender.color.a, trans.a);

            testRender.color = opaque;
            testVis.setVisibilty(false);
            yield return null;
            Assert.Less(testRender.color.a, opaque.a);
        }

        ///<summary> Tests that setVisible will not start an object to fade if already in that state. </summary>
        ///
        [UnityTest]
        public IEnumerator SetVisibleDoesNotStartsSwappingTest()
        {
            testRender.color = trans;
            testVis.setVisibilty(false);
            yield return null;
            Assert.AreEqual(trans.a, testRender.color.a);

            testRender.color = opaque;
            testVis.setVisibilty(true);
            yield return null;
            Assert.AreEqual(opaque.a, testRender.color.a);
        }

        ///<summary> Tests that isVisible correctly identifies is the object is invisible. </summary>
        ///
        [UnityTest]
        public IEnumerator IsVisibleDetectsVisibiltyTest()
        {
            testRender.color = trans;
            yield return null;
            Assert.False(testVis.isVisible());
            
            testRender.color = opaque;
            yield return null;
            Assert.True(testVis.isVisible());
        }

        ///<summary> Tests that isVisible identifies a disabled object as invisible. </summary>
        ///
        [UnityTest]
        public IEnumerator IsVisibleIdentifiesDisablesAsInvisibleTest()
        {
            testRender.color = opaque;
            yield return null;
            Assert.True(testVis.isVisible());

            testObject.SetActive(false);
            Assert.False(testVis.isVisible());
        }
    }
}
