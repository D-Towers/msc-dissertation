﻿//@Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>PlateScriptTests </c> runs tests associated with PlateScript. </summary>
    ///
    public class PlateScriptTests
    {
        GameObject testObject;
        plateScript testPlate;
        GameObject[] testStains;

        ///<summary> Creates gameObjects before each test that includes the necessary components to test PlateScript. </summary>
        ///
        [SetUp]
        public void SetUp()
        {
            testObject = new GameObject();
            testStains = new GameObject[4];
            for (int i = 0; i < 4; i++)
            {
                testStains[i] = new GameObject();
                testStains[i].AddComponent<stainScript>();
                testStains[i].AddComponent<BoxCollider>();
                testStains[i].AddComponent<SpriteRenderer>();
                testStains[i].transform.parent = testObject.transform;
            }
            testObject.SetActive(false);
            testPlate = testObject.AddComponent<plateScript>();
            testObject.SetActive(true);
        }

        ///<summary> Destroys objects after use </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testPlate);
            foreach (GameObject go in testStains)
                GameObject.Destroy(go);
        }

        ///<summary> Tests that the stains have their alpha reset to 1. </summary>
        ///
        [UnityTest]
        public IEnumerator ResetStainsTest()
        {
            foreach(GameObject go in testStains)
                go.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            testPlate.resetStains();
            yield return null;
            foreach(GameObject go in testStains)
                Assert.AreEqual(1, go.GetComponent<SpriteRenderer>().color.a);
        }

        ///<summary> Tests that the BoxColliders become active. </summary>
        ///
        [UnityTest]
        public IEnumerator ActivateStainsTest()
        {
            foreach (GameObject go in testStains)
                Assert.False(go.GetComponent<BoxCollider>().enabled);
            testPlate.activateStains();
            yield return null;
            foreach (GameObject go in testStains)
                Assert.True(go.GetComponent<BoxCollider>().enabled);
        }

        ///<summary> Tests that it correctly calculates the cleanliness percentage. </summary>
        ///
        [UnityTest]
        public IEnumerator CalculateCleanlinessTest()
        {
            float[] randFloats = new float[testStains.Length];
            float testClean = 0;
            for (int i = 0; i < testStains.Length; i++)
            {
                randFloats[i] = Random.Range(0, 1);
                testClean += randFloats[i];
                testStains[i].GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, randFloats[i]);
            }
            yield return null;
            float cleanliness = testPlate.getCleanliness();
            Assert.AreEqual((int) ((testClean/4) * 100), cleanliness);
        }
    }
}
