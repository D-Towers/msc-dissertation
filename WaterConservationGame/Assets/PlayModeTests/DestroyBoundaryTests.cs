﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>DestroyBoundaryTests </c> runs tests associated with the DestroyByBoundary class. </summary>
    ///
    public class DestroyBoundaryTests
    {
        GameObject testObject;
        GameObject testTarget;
        Vector3 BoundaryPos = new Vector3(10, 10, 10);
        string targetTag = "Untagged";

        ///<summary> Creates a gameObject before each test that includes the necessary components to test the DestroyByBoundary script. </summary>
        ///
        [SetUp]
        public void Setup()
        {
            
            testObject = new GameObject();
            testObject.AddComponent<DestroyByBoundary>();
            testObject.AddComponent<SphereCollider>();
            testObject.GetComponent<DestroyByBoundary>().setM_targetTag(targetTag);
            testObject.transform.position = BoundaryPos;

            testTarget = new GameObject();
            testTarget.AddComponent<SphereCollider>();
            testTarget.GetComponent<SphereCollider>().isTrigger = true;
            testTarget.AddComponent<Rigidbody>();
            testTarget.transform.position = new Vector3(10, 10, 10);
        }

        ///<summary> Destroys objects after use. </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testTarget);
        }

        ///<summary> Tests a GameObject with the target tag is disabled after it leaves the collider. </summary>
        ///
        [UnityTest]
        public IEnumerator TriggerDisablesTargetTest()
        {
            testTarget.tag = targetTag;
            Assert.AreEqual(true, testTarget.activeSelf);

            testTarget.transform.position = BoundaryPos;
            Assert.AreEqual(true, testTarget.activeSelf);

            yield return null;
            testTarget.transform.position = BoundaryPos += new Vector3(0, 100, 0);
            yield return null;
            Assert.AreEqual(false, testTarget.activeSelf);
        }

        ///<summary> Tests a GameObject with a non-target tag is not disabled even after it leaves the collider. </summary>
        ///
        [UnityTest]
        public IEnumerator TriggerDoesntDisableTargetTest()
        {
            testTarget.tag = "Water";
            Assert.AreEqual(true, testTarget.activeSelf);

            testTarget.transform.position = BoundaryPos;
            Assert.AreEqual(true, testTarget.activeSelf);

            yield return null;
            testTarget.transform.position = BoundaryPos += new Vector3(0, 100, 0);
            yield return null;
            Assert.AreNotEqual(false, testTarget.activeSelf);
        }
    }
}
