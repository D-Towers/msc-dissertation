﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>MenuScriptTests </c> runs tests associated with MenuScript. </summary>
    ///
    public class MenuScriptTests
    {
        GameObject testObject;

        ///<summary> Creates a gameObject before each test that includes the necessary components to test MenuScript. </summary>
        ///
        [SetUp]
        public void SetUp()
        {
            testObject = new GameObject();
            testObject.SetActive(false);
            testObject.AddComponent<MenuScript>();
            PlayerPrefs.DeleteAll();
        }

        ///<summary> Destroys objects after use </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
        }

        ///<summary> Tests playerprefs are given default value if they are not set. </summary>
        ///
        [UnityTest]
        public IEnumerator SetsDefaultPrefTest()
        {
            testObject.SetActive(true);
            yield return null;
            Assert.AreEqual(0, PlayerPrefs.GetInt("brushScore", 1));
            Assert.AreEqual(0, PlayerPrefs.GetInt("dishScore", 1));
            Assert.AreEqual(1, PlayerPrefs.GetFloat("Volume", 0));
        }

        ///<summary> Tests that the score values are always reset. </summary>
        ///
        [UnityTest]
        public IEnumerator ResetsPlayerPrefTest()
        {
            PlayerPrefs.SetInt("brushScore", 5);
            PlayerPrefs.SetInt("dishScore", 5);
            testObject.SetActive(true);
            yield return null;
            Assert.AreEqual(0, PlayerPrefs.GetInt("brushScore", 5));
            Assert.AreEqual(0, PlayerPrefs.GetInt("dishScore", 5));
        }

        ///<summary> Tests that the volume does not get reset if it has a value. </summary>
        ///
        [UnityTest]
        public IEnumerator DoesNotAlterVolumeTest()
        {
            PlayerPrefs.SetFloat("Volume", 0.75f);
            testObject.SetActive(true);
            yield return null;
            Assert.AreNotEqual(1, PlayerPrefs.GetFloat("Volume", 1));
            Assert.AreEqual(0.75f, PlayerPrefs.GetFloat("Volume", 0));
        }
    }
}
