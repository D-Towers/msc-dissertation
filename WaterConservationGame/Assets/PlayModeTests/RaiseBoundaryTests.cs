﻿//@Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    ///<summary> Test class <c>RaiseBoundaryTests </c> runs tests associated with RaiseBoundary class. </summary>
    ///
    public class RaiseBoundaryTests
    {
        GameObject testObject;
        RaiseBoundary testBoun;

        ///<summary> Creates a gameObject before each test that includes the necessary components to test RaiseBoundary. </summary>
        ///
        [SetUp]
        public void SetUp()
        {
            testObject = new GameObject();
            testBoun = testObject.AddComponent<RaiseBoundary>();
        }

        ///<summary> Destroys objects after use </summary>
        ///
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(testObject);
            GameObject.Destroy(testBoun);
        }

        ///<summary> Tests that if boundary would not be at the limit in the time it takes to animate, returns false. </summary>
        ///
        [Test]
        public void OnlyWouldFinishIfCorrectTimeRemaining()
        {
            testBoun.setLimit(1f);
            Assert.False(testBoun.finishWithAnim());
        }

        ///<summary> Tests that if boundary would be at the limit exactly at the time it takes to animate, returns true. </summary>
        ///
        [Test]
        public void CorrectlyIdentifiesWouldFinishAfterAnimTest()
        {
            //0.0685f = (SPEED) 0.05f * (ANIM_TIME) 1.37f
            testBoun.setLimit(0.0685f);
            Assert.True(testBoun.finishWithAnim());
        }

        ///<summary> Tests that if boundary would be after limit at the time it takes to animate, returns true. </summary>
        ///
        [Test]
        public void CorrectlyIdentifiesWouldFinishGreaterThanAnimTest()
        {
            testBoun.setLimit(0.01f);
            Assert.True(testBoun.finishWithAnim());
        }
    }
}
