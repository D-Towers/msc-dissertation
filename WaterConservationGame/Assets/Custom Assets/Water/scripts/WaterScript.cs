﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>Class <c>WaterScript </c>Controls the movement of the water object it is attached to.</summary>
///
public class WaterScript : MonoBehaviour
{
    private readonly Vector3 VELOCITY = new Vector3(0, -2, 0);

    ///<summary>Called once per frame. Moves the <c>GameObject </c> downward at a rate of 2 units a second.</summary>
    ///
    void Update()
    {
        gameObject.transform.Translate(VELOCITY * Time.deltaTime);
    }
}
