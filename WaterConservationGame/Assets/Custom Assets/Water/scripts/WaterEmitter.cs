﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>WaterEmitter </c> controls the emission of water, and tracks the time water is emitted for.</summary>
///
public class WaterEmitter : MonoBehaviour
{
    [SerializeField] private float m_timeBetweenEmits;

    private AudioSource audioSource;
    private float timePassed = 0;
    private float totalTimeRunning = 0;
    private bool tapIsOn = false;

    ///<summary>Called once when class is initialised. Initialises audioSource for later use.</summary>
    ///
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }
    
    ///<summary>Called once per frame. If tapIsOn is true, calls <c>emitWater</c>.</summary>
    ///
    void Update()
    {
        if (isTapOn()) 
        {
            emitWater();
        }
    }

    ///<summary>This method returns true if water is being emitted and false otherwise.</summary>
    ///<returns>A bool stating whether water is being emitted.</returns>
    ///
    public bool isTapOn()
    {
        return tapIsOn;
    }

    ///<summary>This method adds delta time to the total runtime, and emits a water drop 
    ///from pooled objects if enough time between emits has past.</summary>
    ///
    void emitWater()
    {
        totalTimeRunning += Time.deltaTime;
        timePassed += Time.deltaTime;
        if (timePassed > m_timeBetweenEmits) 
        {
            GameObject water = Pooler.SharedInstance.GetPooledObject();
            if (water != null) 
            {
                water.transform.position = this.transform.position;
                water.SetActive(true);
            }
            timePassed = 0;
        }
    }

    ///<summary>This method changes the bool inicating whether the tap is on, and either pauses or
    ///plays audio depending on the bool.</summary> 
    ///
    public void swapPosition() 
    {
        tapIsOn = !tapIsOn;
        if (tapIsOn) 
        {
            timePassed = 0;
            audioSource.Play();
        }
        else
            audioSource.Stop();
    }

    ///<summary>This method returns the total time water has been emitting for.</summary>
    ///<returns>A float that value is equivelent to the time the water has been emitting in seconds.</returns>
    ///
    public float getTimeRunning() 
    {
        return totalTimeRunning;
    }

    public float getM_timeBetweenEmits()
    {
        return this.m_timeBetweenEmits;
    }

    public void setM_timeBetweenEmits(float m_timeBetweenEmits)
    {
        this.m_timeBetweenEmits = m_timeBetweenEmits;
    }
}
