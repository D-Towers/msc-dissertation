﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

///<summary> Class <c>finalScript </c> is used to show the players final score. </summary>
///
public class finalScript : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreField;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private int m_sceneToLoad;
    private float brushScore, dishScore, finalScore, timePassed = 0;
    private bool displayed = false;

    ///<summary> Called once when class is initialised. Retrieves scores and adds them together. </summary>
    ///    
    void Start()
    {
        brushScore = PlayerPrefs.GetInt("brushScore", 0);
        dishScore = PlayerPrefs.GetInt("dishScore", 0);   
        finalScore = brushScore + dishScore; 
    }

    ///<summary> Called once per frame. Shows player their final score then returns to the main menu. </summary>
    ///
    void Update()
    {
        timePassed += Time.deltaTime;
        if (!displayed && timePassed > 2)
        {
            scoreField.SetText(finalScore.ToString());
            audioSource.Play();
            displayed = true;
        }
        //Checking if it is greater than -1 is mainly for testing purposes to prevent the scene switching if test scene runs too long
        if (displayed && timePassed > 4 && m_sceneToLoad > -1)
            SceneManager.LoadSceneAsync(m_sceneToLoad);
    }

    public float getFinalScore()
    {
        return this.finalScore;
    }

    public void setScoreField(TextMeshProUGUI scoreField)
    {
        this.scoreField = scoreField;
    }

    public void setAudioSource(AudioSource audioSource)
    {
        this.audioSource = audioSource;
    }

    public void setM_sceneToLoad(int m_sceneToLoad)
    {
        this.m_sceneToLoad = m_sceneToLoad;
    }
}
