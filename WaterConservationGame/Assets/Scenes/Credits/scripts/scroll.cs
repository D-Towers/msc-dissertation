﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>scroll </c> controls each element in the credits, moves them upwards and sends the to the bottom when off-screen.</summary>
///
public class scroll : MonoBehaviour
{
    private List<Transform> children;
    [SerializeField] private float m_speed;
    private float yMax;

    ///<summary>Called once when class is initialised. Finds all children and places them into a list.</summary>
    ///
    void Start()
    {
        children = new List<Transform>();
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            children.Insert(i, gameObject.transform.GetChild(i));
        }
        yMax = gameObject.transform.position.y + 105;
    }

    ///<summary>Called once per frame. Moves each element upwards, if an element is above the max, calls getAbove.</summary>
    ///
    void Update()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            Transform tra = children[i];
            tra.Translate(0, Time.deltaTime * m_speed, 0, Space.World);
            if (tra.gameObject.transform.position.y > yMax)
            {
                Vector3 abvPos = getAbove(i).position;
                tra.position = new Vector3(abvPos.x, abvPos.y - 105, abvPos.z);
            }
        }
    }

    ///<summary>Finds the element before current one, and moves below it. </summary>
    ///
    Transform getAbove(int lPos)
    {
        int idx = lPos - 1;
        if (idx < 0)
            idx = children.Count - 1;
        return children[idx];
    }
}
