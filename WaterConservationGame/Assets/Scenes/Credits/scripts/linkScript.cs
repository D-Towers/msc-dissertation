﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

///<summary> Class <c>linkScript </c> is used to provide links to the assets used in the credits.</summary>
///
public class linkScript : MonoBehaviour
{
    ///<summary> Opens a browser page to <paramref name="url"/> destination. </summary>
    ///<param name="url"> A string representation of the URL of the webpage. </param>
    ///
    public void goToLink(string url)
    {
        if (url == "")
        {
            url = gameObject.GetComponent<TextMeshProUGUI>().text;
        }
        Application.OpenURL(url);
    }
}
