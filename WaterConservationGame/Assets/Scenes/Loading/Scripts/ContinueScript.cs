﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

///<summary> Class <c>ContinueScript </c> prevents the games from starting until the user clicks the mouse. </summary>
///
public class ContinueScript : MonoBehaviour
{
    private float timePassed = 0;
    private TextMeshProUGUI textMesh;
    private Color baseCol;

    [SerializeField] private RawImage background;
    [SerializeField] private GameObject parent;
    [SerializeField] private TextMeshProUGUI title, instructions;

    private const float HANG_TIME = 0.2f;

    //As the object exists originally in scenes with no controllers, and both scenes have unique controllers to access them,
    //this static variable is used to be accessable from both controllers
    private static bool continued = false;

    ///<summary> Called once when class is enabled. Initialises variables for later use. </summary>
    ///
    void OnEnable()
    {
        textMesh = gameObject.GetComponent<TextMeshProUGUI>();
        baseCol = textMesh.color;
        continued = false;
    }

    ///<summary> Called once per frame. If player hasn't coninued fade in and out the message asking them to continue, else fade into game. </summary>
    ///
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            continued = true;
        if (continued)
            fadeOut();
        else
        {
            timePassed = Mathf.PingPong(Time.time, 1 + HANG_TIME);
            Color newCol = baseCol;
            newCol.a = timePassed;
            textMesh.color = newCol;
        }
    }

    ///<summary> Fades out canvas, destroying it once everythign has faded out </summary>
    ///
    void fadeOut()
    {
        background.color = makeColTransparent(background.color);
        title.color = makeColTransparent(title.color);
        instructions.color = makeColTransparent(instructions.color);
        textMesh.color = makeColTransparent(textMesh.color);
        if (background.color.a == 0)
            GameObject.Destroy(parent);
    }

    ///<summary> reduces alpha of recieved color <paramref name="old"/> </summary>
    ///<param name="old"> Color to have alpha reduced </param>
    ///<returns> Color which equals <paramref name="old"/> with reduced alpha </returns>
    ///
    Color makeColTransparent(Color old)
    {
        old.a -= Time.deltaTime;
        if (old.a < 0)
            old.a = 0;
        return old;
    }

    ///<summary> Checks if user has continued. </summary>
    ///<returns> True is player has clicked to continue, false otherwise. </returns>
    ///
    public static bool canContinue()
    {
        return continued;
    }
}
