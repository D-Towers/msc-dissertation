﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

///<summary> Class <c>LoadingScript </c> plays animation while loading scene. </summary>
///
public class LoadingScript : MonoBehaviour
{
    private const string LOADING = "Loading";
    private const int MAX_ELIPSES_PLUS = 4;

    private float timePassed = 0;
    private string origScene = "";

    [SerializeField] private int sceneToLoad;
    [SerializeField] private GameObject cont;

    ///<summary> Called once when class is initialised. Stores name of original scene, and starts loading the next scene. </summary>
    ///
    void Start()
    {
        origScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadSceneAsync(sceneToLoad);
    }

    ///<summary> Called once per frame. Plays elipses animation while loading, and fades out loading text after loading. </summary>
    ///
    void Update()
    { 
        if (SceneManager.GetActiveScene().name == origScene)
            whileLoading();
        else
            afterLoading();
    }

    ///<summary> Animates loading text by changing number of elipses shown. </summary>
    ///
    void whileLoading()
    {
        timePassed += Time.deltaTime;
        if (timePassed >= MAX_ELIPSES_PLUS)
            timePassed = 0;
        string elipses = "";
        for (int i = 0; i < (int)(timePassed % MAX_ELIPSES_PLUS); i++)
            elipses += ".";
        gameObject.GetComponent<TextMeshProUGUI>().SetText(LOADING + elipses);
    }

    ///<summary> Activates ContinueScript, and fades out. </summary>
    ///
    void afterLoading()
    {
        cont.SetActive(true);
        fadeOut();
    }

    ///<summary> Fades out the loading text. </summary>
    ///
    void fadeOut()
    {
        Color old = gameObject.GetComponent<TextMeshProUGUI>().color;
        old.a -= Time.deltaTime;
        gameObject.GetComponent<TextMeshProUGUI>().color = old;
        if (old.a <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
