﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>RaiseBoundary </c> manages the boundary which rises with the water level. </summary>
///
public class RaiseBoundary : MonoBehaviour
{
    [SerializeField] private float limit;

    private float distance = 0;

    private const float SPEED = 0.05f;
    private const float ANIM_TIME = 1.37f;

    ///<summary> Called once per frame. Raises the boundary to limit, ensuring it does not go over limit. </summary>
    ///
    void Update()
    {
        if (limit > distance && DishesController.dishesController.checkPlugged() && DishesController.dishesController.checkTapRunning())
        {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            float moved = SPEED * Time.deltaTime;
            if (distance + moved > limit)
                moved -= (limit - distance);
            distance += moved;
            gameObject.transform.Translate(new Vector3(0, moved, 0), Space.World);
        }
    }

    ///<summary> Checks whether the animation would finish aronud the time the limit would be reached. </summary>
    ///<returns> True if the animation would finish as/after the water reached the limit, false otherwise. </returns>
    ///
    public bool finishWithAnim()
    {
        if (distance + (SPEED * ANIM_TIME) > limit)
            return true;
        return false;
    }

    public float getLimit()
    {
        return this.limit;
    }

    public void setLimit(float limit)
    {
        this.limit = limit;
    }
}
