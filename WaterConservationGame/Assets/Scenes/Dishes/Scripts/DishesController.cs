﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


///<summary> Class <c>DishesController </c> inherits from <c>SceneController </c>. This class controls
/// the functions to run the Dishes scene. </summary>
///
public class DishesController : SceneController
{
    private const string PLUG = "ThePlug";
    private const string PLATE = "plate";
    private const string TAP = "tap";
    private const string STAIN = "stain";
    private const int MAX_PLATES = 10;
    private const int MOUSE_DIVIDER = 50;
    private readonly Vector3 FALSE_COORDS = new Vector3(-1000, -1000, -1000);

    private bool turnedOffTap = false;
    private bool plateInSink = false;
    private bool plateReady = true;
    private bool plateTriggered = false;
    private List<GameObject> rackedPlates = new List<GameObject>();
    private int platesRacked = 0;

    [SerializeField] private ObjectChecker m_plugChecker;
    [SerializeField] private GameObject m_plate;
    [SerializeField] private TextMeshProUGUI m_cleanlinessHUD;
    [SerializeField] private GameObject m_sponge;
    [SerializeField] private GameObject m_plateStackCopy;
    [SerializeField] private GameObject m_platePrefab;
    [SerializeField] private RaiseBoundary m_boundary;
    
    public static DishesController dishesController;

    ///<summary> Called once when class is initialised. Initialises dishesController for later use throughout scripts in scene. </summary>
    ///
    private void Awake() {
        dishesController = this;
    }

    ///<summary> Called once per frame. Calls parent Update method. If game is not paused, manages actions that can be done in the scene. </summary>
    ///
    protected override void Update()
    {
        base.Update();
        if (isRunning() && Time.timeScale != 0)
        {
            if (checkTapRunning() && checkPlugged() && !turnedOffTap)
                turnOffTap();
            else if (plateInSink)
               washActions();
            else if (plateTriggered && plateReady)
                activatePlateMove();
            else if (!plateTriggered)
                performActions();
        }
    }

    ///<sumary> Checks whether the tap is on. </summary>
    ///<returns> True if tap is running, false otherwise </returns>
    ///
    public bool checkTapRunning()
    {
        return  m_waterEmitter.isTapOn();
    }

    ///<sumary> Checks whether sink has been plugged. </summary>
    ///<returns> True if sink has been plugged, false otherwise </returns>
    ///
    public bool checkPlugged()
    {
        return m_plugChecker.ObjectsTriggered();
    }

    ///<sumary> Turns the tap off if the sink is plugged, before the sink overflows. </summary>
    ///
    void turnOffTap()
    {
        if (m_boundary.finishWithAnim())
        {
            animator.SetTrigger("turnTap");
            turnedOffTap = true;
        }
    }

    ///<summary> Manages actions when plate is being cleaned. </summary>
    void washActions()
    {
        cleanPlate();
        moveSponge();
        if (m_plate.GetComponent<plateScript>().getCleanliness() == 100)
        {
            m_sponge.SetActive(false);
            triggerRack();
        }
    }

    ///<summary> Uses speed of mouse movement to reduce visibility of stains and update cleanliness for the plate. </summary>
    ///
    private void cleanPlate()
    {
        GameObject objectHit = getRaycastObject("stain");
        if (objectHit != null && objectHit.tag == STAIN)
        {
            float speed = calculateSpeedFromMouse();
            if (speed > 0.01f)
                objectHit.GetComponent<stainScript>().clean(speed);
        }
        setCleanliness(m_plate.GetComponent<plateScript>().getCleanliness());
    }

    ///<summary> Moves the sponge object to cursor position with the bottom facing the plate. </summary>
    ///
    private void moveSponge()
    {
        if (m_sponge.activeSelf == false)
            m_sponge.SetActive(true);
        Vector3 startPoint = getMousePositionInWorld();
        Vector3 endPoint = getRaycastHitCoords();
        if (endPoint == FALSE_COORDS) return;
        m_sponge.transform.SetPositionAndRotation(endPoint, Quaternion.LookRotation(endPoint, startPoint * -1));
    }

    ///<summary> Called whenever a plate is to be moved to the rack, and performs the necessary baheviour.</summary>
    ///
    private void triggerRack()
    {
        movePlatesBack();
        m_plateStackCopy.SetActive(true);
        doubleTrigger("rackPlate", "isRacked");
        plateInSink = false;
        updateScore(1000);
        platesRacked++;
        if (platesRacked >= MAX_PLATES)
            canEnd = true;
    }

    ///<summary> Since triggers for animations are split to avoid errors, calls two triggers at the same time. </summary>
    ///<param name="triggerOne"> String that is the name of the first trigger in the animator to be triggered </param>
    ///<param name="triggerTwo"> String that is the name of the Second trigger in the animator to be triggered </param>
    ///
    void doubleTrigger(string triggerOne, string triggerTwo)
    {
        animator.SetTrigger(triggerOne);
        animator.SetTrigger(triggerTwo);
    }

    ///<summary> Calculates the speed at which the mouse was moved between this frame and the previous frame. </summary>
    ///<returns> The speed that the mouse has been moved at, divied by MOUSE_DIVIDER. </returns>
    ///
    public float calculateSpeedFromMouse()
    {
        return Mathf.Sqrt(Mathf.Pow(Input.GetAxis("Mouse X"),2) + Mathf.Pow(Input.GetAxis("Mouse Y"),2))/MOUSE_DIVIDER;
    }

    ///<summary> Sets when the plate is ready to perform animations again. Used to keep animations in sync. </summary>
    ///
    public void setPlateAsReady()
    {
        plateReady = true;
    }

    ///<summary> Allows other classes to change the plateInSink bool to determine behaviour of plates.</summary>
    ///<param name="inSink"> Desired value of the plateInSink bool </param>
    ///
    public void setPlateInSink(bool inSink)
    {
        plateInSink = inSink;
    }

    ///<summary> Creates a new plate in position of the animated plate, and makes the animated plate invisible so it can 
    ///be returned to its original position. </summary>
    ///
    public void hidePlate()
    {
        m_plate.SetActive(false);
        GameObject newPlate = GameObject.Instantiate(m_platePrefab, m_plate.transform.position, m_plate.transform.rotation);
        rackedPlates.Add(newPlate);
    }

    ///<summary> Moves all added plates backwards on the rack, so the look like they are filling up the rack. </summary>
    ///
    public void movePlatesBack()
    {
        foreach (GameObject plate in rackedPlates)
        {
            plate.transform.Translate(new Vector3(-0.05f, 0, 0), Space.World);
        }
    }

    ///<summary> Enables the animated plate while disabling the placeholder. </summary>
    ///
    public void revealPlate()
    {
        m_plate.GetComponent<plateScript>().resetStains();
        m_plate.SetActive(true);
        m_plateStackCopy.SetActive(false);
        plateReady = true;
    }


    ///<summary> Checks if an interactable has been clicked on, if something has been interacted with, 
    ///it will perform the unique behaviour</summary>
    ///
    private void performActions() 
    {
        Cursor.lockState = CursorLockMode.None;
        if (Input.GetMouseButtonDown(0))
        {
            switch (getRaycastString())
            {
                case PLUG:
                    doubleTrigger("grabPlug", "plugMove");
                    break;
                case PLATE:
                    plateAction();
                    break;
                case TAP:
                    animator.SetTrigger("turnTap");
                    break;
            }
        }
    }

    ///<summary> checks whether plate is a valid action, the checks if plate is ready to move, if it isn't, a trigger is set. </summary>
    ///
    void plateAction()
    {
        if ((!checkPlugged() && checkTapRunning() || checkPlugged()))
        {
            if (!plateReady)
                plateTriggered = true;
            else
                activatePlateMove();
        }
    }

    ///<summary> triggers animations to move plate into sink, resets bools associated with this event, and activates the stains. </summary>
    ///
    private void activatePlateMove()
    {
        doubleTrigger("grabPlate", "isGrabbed");
        m_plate.GetComponent<plateScript>().activateStains();
        plateReady = false;
        plateTriggered = false;
    }

    ///<summary> Calculates the 3D world position of the cursor. </summary>
    ///
    private Vector3 getMousePositionInWorld()
    {
        Vector3 input = Input.mousePosition;
        input.z = 1; //Needed to get output in 3D.
        return Camera.main.ScreenToWorldPoint(input);
    }

    ///<summary> Performs a raycast, and returns the hit gameObject. </summary>
    ///<param name="mask">Default is "Interactable". String which is equal to the name of the desired layer to check against. </param>
    ///<returns>The gameObject hit by the raycast or null, if nothing on the layer was hit. </returns> 
    ///
    private GameObject getRaycastObject(string mask = "Interactable")
    {
        RaycastHit hit = new RaycastHit();
        if (raycastScene(ref hit, mask))
        {
            return hit.transform.gameObject;
        }
        else return null;
    }

    ///<summary> Performs a raycast, and returns the tag or name of the hit gameObject. </summary>
    ///<param name="mask">Default is "Interactable". String which is equal to the name of the desired layer to check against. </param>
    ///<returns>If a gameObject was hit, a string containing the tag of the gameObject or the name if gameObject was untagged.
    ///If no gameObject was hit, an empty string. </returns> 
    ///
    private string getRaycastString(string mask = "Interactable")
    {
        GameObject go = getRaycastObject(mask);
        if (go == null)
            return "";
        if (!go.transform.CompareTag("Untagged"))
            return go.transform.tag;
        else
            return go.transform.name;
    }

    ///<summary> Performs a raycast, and returns the hit gameObject. </summary>
    ///<param name="mask">Default is "Interactable". String which is equal to the name of the desired layer to check against. </param>
    ///<returns>The gameObject hit by the raycast or null, if nothing on the layer was hit. </returns> 
    ///
    private Vector3 getRaycastHitCoords(string mask = "Interactable")
    {
        RaycastHit hit = new RaycastHit();
        if (raycastScene(ref hit, mask))
        {
            return hit.point;
        }
        return FALSE_COORDS;
    }

    ///<summary> Performs a raycast and returns the results into the RaycastHit <paramref name="hit"/> that is passed through. </summary>
    ///<param name="hit"> The object that the result of the raycast will be returned to. </param>
    ///<param name="mask"> Default is "Interactable". String which is equal to the name of the desired layer to check against. </param>
    ///
    private bool raycastScene(ref RaycastHit hit, string mask)
    {
        Vector3 startPoint = getMousePositionInWorld();
        Vector3 forward = startPoint - Camera.main.transform.position;
        if (Physics.Raycast(startPoint, startPoint - Camera.main.transform.position, out hit, Mathf.Infinity, LayerMask.GetMask(mask)))
            return true;
        else 
            return false;
    }

    ///<summary> Checks whether the plate is in the sink. </summary>
    ///<returns> True if plate is in the sink, false otherwise </returns>
    ///
    public bool isPlateInSink()
    {
        return plateInSink;
    }
}