﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>TrickPlates </c> is used to reset the plate without the player knowing. </summary>
///
public class TrickPlates : StateMachineBehaviour
{
    ///<summary> Called when a transition starts and this state is evaluated. Makes the plate invisible and moves it without being seen. </summary>
    ///
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        DishesController.dishesController.hidePlate();
    }

    ///<summary> Called when a transition ends and this state is no longer evaluating. Makes the plate visible again. </summary>
    ///
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        DishesController.dishesController.revealPlate();
    }
}
