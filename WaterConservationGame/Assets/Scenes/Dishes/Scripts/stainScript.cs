﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>stainScript </c> This class controls the functions for the stains on the plate. </summary>
///
public class stainScript : MonoBehaviour
{
    private Color startColor;
    private Color currColor;

    ///<summary> Called once when class is initialised. Initialises the startColor and currColor for this stain. </summary>
    ///
    void Start()
    {
        startColor = gameObject.GetComponent<SpriteRenderer>().color;
        currColor = startColor;
    }

    ///<summary> Called once per frame. Changes the color of this stain to currCol, 
    /// if the current color has an alpha of 0, disables the stain. </summary>
    ///
    void Update()
    {
        gameObject.GetComponent<SpriteRenderer>().color = currColor;
        if (currColor.a == 0)
            gameObject.GetComponent<BoxCollider>().enabled = false;
    }

    ///<summary> Reduces the alpha of the stain by the amount given. </summary>
    ///<param name="val"> the amount that the alpha is reduced by. </param>
    ///
    public void clean(float val)
    {
        if (currColor.a - val < 0)
            currColor.a = 0;
        else
            currColor.a -= val;
    }

    ///<summary> sets the color of the stain back to the original color. </summary>
    ///
    public void reset()
    {
        currColor = startColor;
    }

    public Color getStartColor()
    {
        return this.startColor;
    }

    public void setStartColor(Color startColor)
    {
        this.startColor = startColor;
    }

    public Color getCurrColor()
    {
        return this.currColor;
    }

    public void setCurrColor(Color currColor)
    {
        this.currColor = currColor;
    }
}
