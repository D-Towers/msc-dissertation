﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>spongeScript </c> manages the SFX emitted by the sponge while cleaning. </summary>
///
public class spongeScript : MonoBehaviour
{

    [SerializeField] private AudioClip sinkFullSound;

    private AudioSource audioSource;
    private float timeBetween = 0;
    private Vector3 posLastFrame;

    private const float MAX_TIME_BETWEEN = 0.04f;
    

    ///<summary> Called once when class is initialised. Initilises the AudioSource used, and sets an initial position.</summary>
    ///
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        posLastFrame = gameObject.transform.position;
    }

    ///<summary> Called once per frame. Plays Audio when sponge moves. 
    ///Pauses audio if a period of time without moving the sponge elapses. </summary>
    ///
    void Update()
    {
        if (audioSource.clip != sinkFullSound && DishesController.dishesController.checkPlugged())
            audioSource.clip = sinkFullSound;
        if (DishesController.dishesController.isPlateInSink() && posLastFrame != gameObject.transform.position)
        {
            if(!audioSource.isPlaying)
                audioSource.Play();
            timeBetween = 0;
        }
        else if (audioSource.isPlaying)
        {
            timeBetween += Time.deltaTime;
            if (timeBetween > MAX_TIME_BETWEEN)
                audioSource.Pause();
        }
        posLastFrame = gameObject.transform.position;
    }
}
