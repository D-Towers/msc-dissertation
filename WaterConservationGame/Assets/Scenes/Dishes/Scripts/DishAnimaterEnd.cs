﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>DishAnimaterEnd </c> is used to trigger the scene to end from an animation state. </summary>
///
public class DishAnimaterEnd : StateMachineBehaviour
{
    ///<summary> Called when a transition starts and this state is evaluated. Checks if criteria to end is met, if so ends the scene. </summary>
    ///
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (DishesController.dishesController.canBeEnded())
            DishesController.dishesController.endScene();
    }
}
