﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>setSinkTrigger </c> sets the plate as in the sink once the animation has finished. </summary>
///
public class setSinkTrigger : StateMachineBehaviour
{
    [SerializeField] SceneController m_test;

    ///<summary>Called when a transition starts and this state is evaluated. Lets the sceneController know the plate is in the sink. </summary>
    ///
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        DishesController.dishesController.setPlateInSink(true);
    }
}
