﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>plateScript </c> manages the stains on the plate. </summary>
///
public class plateScript : MonoBehaviour
{
    GameObject[] stains = new GameObject[4];

    ///<summary> Called once when class is initialised. Saves the stains in an array for later use.</summary>
    ///
    void Start()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            stains[i] = gameObject.transform.GetChild(i).gameObject;
            stains[i].GetComponent<BoxCollider>().enabled = false;
        }
    }

    ///<summary> Enables the colliders of the stains allowing them to be detected by the raycasts. </summary>
    ///
    public void activateStains()
    {
        for (int i = 0; i < stains.Length; i ++)
            stains[i].GetComponent<BoxCollider>().enabled = true;
    }

    ///<summary> Resets the alpha of each stain back to 1. </summary>
    ///
    public void resetStains()
    {
        foreach(GameObject go in stains)
            go.GetComponent<stainScript>().reset();;
    }

    ///<summary> Calculates the cleanliness of the plate based upon how visible each of the stains are. </summary>
    ///<returns> A integer between 0 and 100 which is how invisible all the stains are as a percent. e.g. all 4 being invisible is 100%. </returns>
    ///
    public int getCleanliness()
    {
        float remainingStains = 0;
        foreach(GameObject go in stains)
            remainingStains += go.GetComponent<SpriteRenderer>().color.a;
        return (int) (100 - ((remainingStains * 100)/4));
    }
}
