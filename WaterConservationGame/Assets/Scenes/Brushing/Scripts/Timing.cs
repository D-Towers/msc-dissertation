﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

///<summary> Class <c>Timing </c> is used to manage the scoring mechanics of the brushing game.</summary>
public class Timing : MonoBehaviour
{
    private GameObject[] titleArr;
    private static float shrinkSpeed = 2;

    private readonly Vector3 RESET_SCALE = new Vector3(2.5f,2.5f,1); 
    private const int BASE_SPEED = 2;
    private const int BASE_SCORE = 50;
    private const float SHRINK_SPEED_INC = 0.1f;
    private const float LOWER_SCALE_LIMIT = 0.5f;
    private const float NEGATIVE_PENALTY = 2;
    private const float EXTRA_TITLE_HEIGHT = 0.15f;

    [SerializeField] private GameObject m_perfectSprite;
    [SerializeField] private GameObject m_excellentSprite;
    [SerializeField] private GameObject m_goodSprite;
    [SerializeField] private GameObject m_okaySprite;
    [SerializeField] private GameObject m_badSprite;
    [SerializeField] private GameObject m_missedSprite;

    enum grades
    {
        Perfect,
        Excellent,
        GOOD,
        OKAY,
        BAD,
        MISSED
    }

    ///<summary>Called once when class is initialised. Sets self as inactive, and sets up array for later.</summary>
    ///
    void Start()
    {
        gameObject.SetActive(false);
        titleArr = new GameObject[6]{m_perfectSprite, m_excellentSprite, m_goodSprite, m_okaySprite, m_badSprite, m_missedSprite};
    }

    ///<summary>Called once per frame. Manages speed and scale of the golg "ring" that is used to indicate timing.</summary>
    ///
    void Update()
    {
       if (gameObject.transform.localScale.x > LOWER_SCALE_LIMIT) 
       {
           Vector3 scale = gameObject.transform.localScale;
           float scaDec = Time.deltaTime * shrinkSpeed;
           gameObject.transform.localScale = new Vector3(scale.x - scaDec, scale.y - scaDec, scale.z);
       }
       else
       {
           shrinkSpeed = BASE_SPEED;
           showScoreTitle(Enum.GetNames(typeof(grades)).Length - 1);
           BrushController.brushController.updateScore(-BASE_SCORE);
           reset();
       }
    }

    ///<summary> Creates a game object of the correct score title. </summary>
    ///
    private void showScoreTitle(int grade) 
    {
        GameObject obj = (GameObject) Instantiate(titleArr[grade]);
        Vector3 pos = gameObject.transform.position;
        obj.transform.SetPositionAndRotation(new Vector3(pos.x, pos.y + EXTRA_TITLE_HEIGHT, pos.z), obj.transform.rotation);
    }

    ///<summary> Used to awaken the gameObject and set it back to the max scale.</summary>
    ///
    public void reset() 
    {
        gameObject.SetActive(true);
        gameObject.transform.localScale = RESET_SCALE;
    }

    ///<summary> Gets current scale and compares it against 1, then calculates a score </summary>
    ///
    public void checkTiming() 
    {
        grades currGrade = grades.BAD;
        float currScaleDiff = gameObject.transform.localScale.x - 1;
        if (currScaleDiff < 0)
            currScaleDiff *= NEGATIVE_PENALTY;
        currGrade = (grades)Mathf.Clamp(Mathf.Floor(Mathf.Abs(currScaleDiff) * 10), 0, Enum.GetNames(typeof(grades)).Length - 2);
        turnOff();
        showScoreTitle((int) currGrade);
        shrinkSpeed += SHRINK_SPEED_INC;
        BrushController.brushController.updateScore(BASE_SCORE * ((Enum.GetNames(typeof(grades)).Length -1) - (int) currGrade));
    }

    ///<summary> Sets the gameObject to be inactive.</summary>
    ///
    public void turnOff()
    {
        gameObject.SetActive(false);
    }

    public void setM_perfectSprite(GameObject m_perfectSprite)
    {
        this.m_perfectSprite = m_perfectSprite;
    }

    public void setM_excellentSprite(GameObject m_excellentSprite)
    {
        this.m_excellentSprite = m_excellentSprite;
    }

    public void setM_goodSprite(GameObject m_goodSprite)
    {
        this.m_goodSprite = m_goodSprite;
    }

    public void setM_okaySprite(GameObject m_okaySprite)
    {
        this.m_okaySprite = m_okaySprite;
    }

    public void setM_badSprite(GameObject m_badSprite)
    {
        this.m_badSprite = m_badSprite;
    }

    public void setM_missedSprite(GameObject m_missedSprite)
    {
        this.m_missedSprite = m_missedSprite;
    }

    public float getLowerScaleLimit()
    {
        return LOWER_SCALE_LIMIT;
    }
    
    public Vector3 getResetScale()
    {
        return RESET_SCALE;
    }
}
