﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>AnimaterTapTrigger </c> maintains the state of a bool used by the animator controller.</summary>
///
public class AnimaterTapTrigger : StateMachineBehaviour
{
    ///<summary>Called when a transition starts and this state is evaluated. Inverts bool used in the animator to tell if the tap is running
    ///and swaps whether the "E" button image is visible.</summary>
    ///
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("TapRunning",!animator.GetBool("TapRunning"));
        BrushController.brushController.triggerWater();
    }
}
