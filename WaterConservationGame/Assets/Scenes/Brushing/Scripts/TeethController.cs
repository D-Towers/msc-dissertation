﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>TeethController </c> used as a camera and action controller in the Brushing minigame.</summary>
///
public class TeethController : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private float m_secDelayed;
    [SerializeField] private GameObject m_foam;
    [SerializeField] private SwapVisibility m_upUI;
    [SerializeField] private SwapVisibility m_downUI;
    [SerializeField] private SwapVisibility m_leftUI;
    [SerializeField] private SwapVisibility m_rightUI;
    [SerializeField] private SwapVisibility m_rinseUI;

    private readonly int lookDownState = Animator.StringToHash("Look Down");
    private readonly int readyState = Animator.StringToHash("Ready");
    private readonly int leftState = Animator.StringToHash("Left");
    private readonly int rightState = Animator.StringToHash("Right");
    private readonly int rinseState = Animator.StringToHash("RinseBrush");

    private Vector3 start;
    private float timePassed, buffer = 0;
    private int timesBrushed = 0;
    private Timing lt, rt;
    private bool rinsed = false;

    private const int FOAM_SPEED_PENALTY = 75;
    private const float MAX_FOAM_SIZE = 0.05f;
    private const int BUFFER_LIMIT_MAX = 2;
    private const int BUFFER_LIMIT_MIN = 0;
    private const int BUFFER_GROW_TRIG = 1;
    private const float BRUSH_TIME = 0.55f;
    private const float BRUSH_TIME_BUFFER = 0.15f;
    private const int MAX_TIMES_BRUSHED = 30;

    ///<summary>Called once when class is initialised. Initialises objects for later use.</summary>
    ///
    void Start()
    {
        start = m_foam.transform.localScale;
        lt = m_leftUI.transform.GetChild(0).GetComponent<Timing>();
        rt = m_rightUI.transform.GetChild(0).GetComponent<Timing>();
    }

    ///<summary>Called once per frame. If the game is running, manages the actions in each state.</summary>
    ///
    void Update()
    {
        if (Time.timeScale > 0)
        {
            timePassed = Mathf.Clamp(timePassed -= Time.deltaTime, 0, 1);
            updateFoam();
            int currState = anim.GetCurrentAnimatorStateInfo(0).shortNameHash;
            if(currState == lookDownState)
                downStateFunction();
            else if (currState == readyState) 
                moveBrush(KeyCode.A, rt, null, "Left", m_secDelayed);
            else if (currState == leftState )
                moveBrush(KeyCode.D,  lt,  rt, "Right");
            else if (currState == rightState) 
                moveBrush(KeyCode.A,  rt, lt, "Left");
        }
    }

    ///<summary> Controls the scale of the foam that appearrs when brushing to make the scene more interesting.</summary>
    ///
    private void updateFoam() 
    {
        float scale = m_foam.transform.localScale.x;
        buffer = Mathf.Clamp(buffer - (Time.deltaTime/BUFFER_LIMIT_MAX), BUFFER_LIMIT_MIN, BUFFER_LIMIT_MAX);
        if (buffer >= BUFFER_GROW_TRIG && timePassed > 0 && timePassed < BRUSH_TIME - BRUSH_TIME_BUFFER)
            scale += (Time.deltaTime/FOAM_SPEED_PENALTY);
        else if (buffer == BUFFER_LIMIT_MIN)
            scale -= (Time.deltaTime/FOAM_SPEED_PENALTY);
        scale  = Mathf.Clamp(scale, start.x, MAX_FOAM_SIZE);
        m_foam.transform.localScale = new Vector3(scale, scale, scale);
    }

    ///<summary> Used to reduce the amount of code inside the update block </summary>
    ///
    private void downStateFunction() 
    {
        if (!brushCheck())
            moveHead(KeyCode.W);
        if (Input.GetKeyDown(KeyCode.Q) && BrushController.brushController.isRunning())
            anim.SetTrigger("TurnTap");
        else if (Input.GetKeyDown(KeyCode.E))
        {
            anim.SetTrigger("Brush");
            if (getCleanPerc() == 100)
                rinsed = true;
        }
    }

    ///<summary> Checks whether the player has brushed enough times.</summary>
    ///<returns> A bool which is true if the player has brushed a number of times greater than or equal to MAX_TIMES_BRUSHED </returns>
    ///
    private bool brushCheck()
    {
        return timesBrushed >= MAX_TIMES_BRUSHED;
    }

    ///<summary> Changes the position of the head, and changes which ui elements are visible.</summary>
    ///<param name="kc"> The Keycode of the key which triggers head movement.null </param>
    ///
    private void moveHead(KeyCode kc) 
    {
        if (Input.GetKeyDown(kc)) 
        {
            anim.SetTrigger("MoveHead");
            swapUI();
            rt.turnOff();
            lt.turnOff();
        }
    }

    ///<summary> Calculates a percentage of cleanliness from the times the player has brushed in regards to MAX_TIMES_BRUSHED </summary>
    ///<returns> An int between 0 and 100 which is the percent of timesBrushed compared to MAX_TIMES_BRUSHED </returns>
    ///
    public int getCleanPerc() 
    {
        return Mathf.RoundToInt(timesBrushed * (100f/MAX_TIMES_BRUSHED));
    }

    ///<summary> Swaps which UI is visible, to show which actions are possible to the player. </summary>
    ///
    private void swapUI()
    {
        if (!brushCheck())
        {
            m_upUI.swap();
            m_leftUI.swap();
        }
        m_downUI.swap();
        m_rightUI.setVisibilty(false);
    }

    ///<summary> Controls the 3 states associated with brushing </summary>
    ///<param name="kc">KeyCode to key which triggers scoring.</param>
    ///<param name="rst"> Which button is getting reset. (i.e. turning on).</param>
    ///<param name="chk"> Which button is being scored. (i.e. turning off).</param>
    ///<param name="trig"> Used to indicate which animation trigger to invoke.</param>
    ///<param name="delay"> Amount of time before audio is played, 0 by default.</param>
    ///
    private void moveBrush(KeyCode kc, Timing rst, Timing chk, string trig, float delay = 0){
        if (Input.GetKeyDown(kc) && timesBrushed < MAX_TIMES_BRUSHED) 
        {
            brushOnce();
            if (!m_rightUI.isVisible())
                m_rightUI.setVisibilty(true);
            if (chk != null)
                chk.checkTiming();
            anim.SetTrigger(trig);
            audioSource.PlayDelayed(delay);
            timePassed = BRUSH_TIME;
            if (!brushCheck())
                rst.reset();
            else
            {
                m_leftUI.setVisibilty(false);
                m_rightUI.setVisibilty(false);
            }
        }
        else 
            moveHead(KeyCode.S);
    }

    ///<summary> Increments number of times brushed, and the buffer for foam. Turns off UI once brushed over MAX_TIMES_BRUSHED. </summary>
    ///
    private void brushOnce() 
    {
        buffer++;
        timesBrushed++;
        if (brushCheck())
        {
            rt.turnOff();
            lt.turnOff();
        }
    }

    ///<summary> checks whether the player has rinsed their brush after finishing brushing. </summary>
    ///<returns> true if player has rinsed after brushing, false otherwise. </returns>
    ///
    public bool hasRinsed()
    {
        return rinsed;
    }
}