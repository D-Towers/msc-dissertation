﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>PauseAudio </c> pauses the brushing SFX so it only plays while the brish is moving.</summary>
///
public class PauseAudio : StateMachineBehaviour
{
    ///<summary>Called when a transition ends and this state is finished evaluating. Calls <c>BrushController </c> to pause the SFX.</summary>
    ///
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        BrushController.brushController.pauseAudio();
    }
}
