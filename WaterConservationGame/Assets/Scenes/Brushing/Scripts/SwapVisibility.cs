﻿// @Author: David Towers (160243066)
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>SwapVisibility </c> Manages behaviour needed in the main menu. </summary>
///
public class SwapVisibility : MonoBehaviour
{
    private SpriteRenderer sr;
    bool change, toTrans = false;
    float timePassed = 0;

    const int OPAQUE = 1;
    const int TRANSP = 0;
    const float TIME_MULT = 3;

    ///<summary> Called once when class is initialised. Initialises the SpriteRenderer. </summary>
    ///
    void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();
    }

    ///<summary> Called once per frame. If object has been set to Swap, object will switch visibility. </summary>
    ///
    void Update()
    {
        if (change) 
        {
            timePassed += Time.deltaTime;
            float modi = TIME_MULT * Mathf.Clamp(timePassed, TRANSP, OPAQUE/TIME_MULT); 
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, (toTrans ? OPAQUE - modi: TRANSP + modi));
            if (modi >= OPAQUE)
            {
                change = false;
                timePassed = 0;
            }
        }
    }

    ///<summary> Sets the variables to start the object fading in or fading out.</summary>
    ///
    public void swap() 
    {
        change = true;
        if (sr.color.a == 1)
            toTrans = true;
        else
            toTrans = false;
    }

    ///<summary> Sets the visibility of the object. </summary>
    ///<param name="setVisible">Tells the method whether the object should become visible or invisible.
    /// true equals make visible, false equals make invisible.</param>
    ///
    public void setVisibilty(bool setVisible) 
    {
        if (!gameObject.activeSelf || (setVisible && isVisible()) || (!setVisible && !isVisible())) return;
        swap();
    }

    ///<summary> Checks whether the object is visible in the world. </summary>
    ///<returns> True if object is visible in world, and False otherwise. </returns>
    ///
    public bool isVisible() 
    {
        if (gameObject.activeSelf)
            return Convert.ToBoolean(sr.color.a);
        return false;
    }
}
