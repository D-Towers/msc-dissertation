﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

///<summary> Class <c>BrushController </c> inherits from <c>SceneController </c>. This class controls
/// the functions to run the Brushing scene.</summary>
///
public class BrushController : SceneController
{
    [SerializeField] private TeethController m_controller;
    [SerializeField] private AudioSource m_toPause;
    [SerializeField] private SwapVisibility m_brushUI;
    [SerializeField] private ObjectChecker m_brushChecker;
    
    public static BrushController brushController;

    ///<summary>Called once when class is initialised. Initialises brushController for later use throughout scripts in scene.</summary>
    ///
    void Start()
    {
        brushController = this;
    }

    ///<summary>Called once per frame. Calls parent Update method. If game is not paused, cleanliness area on hud will be updated.</summary>
    ///
    protected override void Update()
    {
        base.Update();
        if (isRunning() && Time.timeScale != 0) 
        {
            updateClean();
            if (m_brushChecker.ObjectsTriggered())
                canEnd = true;
        }
    }
    
    ///<summary>Updates the cleanliness field on the HUD.</summary>
    ///
    private void updateClean() 
    {
        m_cleanliness.text = "Cleanliness: " + m_controller.getCleanPerc() + "%";
    }

    ///<summary>This function makes the "E" button image visible.</summary>
    ///
    public void triggerWater()
    {
        m_brushUI.swap();
    }

    ///<summary>This function pauses the Brushing sound effect.</summary>
    ///
    public void pauseAudio()
    {
        m_toPause.Pause();
    }

    ///<summary>This function triggers the end of the scene, if the player has rinsed their brush.</summary>
    ///
    public void triggerEnd() 
    {
        if (m_controller.hasRinsed())
            endScene();
    }
}
