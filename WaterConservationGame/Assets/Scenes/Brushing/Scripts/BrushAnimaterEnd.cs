﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary> Class <c>BrushAnimaterEnd </c> is used to trigger the scene to end from an animation state.</summary>
///
public class BrushAnimaterEnd : StateMachineBehaviour
{
    ///<summary>Called when a transition starts and this state is evaluated. Checks if criteria to end is met, if so ends the scene</summary>
    ///
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (BrushController.brushController.canBeEnded())
            BrushController.brushController.triggerEnd();
    }
}
