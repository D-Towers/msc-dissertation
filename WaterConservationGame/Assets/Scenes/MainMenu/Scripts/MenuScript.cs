﻿// @Author: David Towers (160243066)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

///<summary> Class <c>MenuScript </c> manages behaviour needed in the main menu. </summary>
///
public class MenuScript : MonoBehaviour
{
    ///<summary> Called once when class is initialised. Resets scores and initialises volume if it isn't set. </summary>
    ///
    void Start()
    {
        PlayerPrefs.SetInt("brushScore", 0);
        PlayerPrefs.SetInt("dishScore", 0);
        float vol = PlayerPrefs.GetFloat("Volume", -1f);
        if (vol == -1)
        {
            PlayerPrefs.SetFloat("Volume", 1);
        } 
    }

    ///<summary> Loads the next scene. </summary>
    ///
    public void load(int sceneID)
    {
        SceneManager.LoadSceneAsync(sceneID);
    }

    ///<summary> Quits the application. </summary>
    ///
    public void exit()
    {
        Application.Quit();
    }
}
